﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS.ViewModel
{
    public class ROUTEspecial
    {
       public SBAS.Models.ROUTE ROUTE { get; set; }
        public string start_route_name { get; set; }
        public string end_route_name { get; set; }
    }
}