﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS.ViewModel
{
    public class BUS_DRIVERSandBUSROUTES
    {
       public SBAS.Models.BUS_DRIVERS BUS_DRIVERS { get; set; }
        public SBAS.Models.BUS_ROUTES BUS_ROUTES { get; set; }
    }
}