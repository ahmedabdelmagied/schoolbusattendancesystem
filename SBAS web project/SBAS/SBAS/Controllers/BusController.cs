﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class BusController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /Bus/

        public ActionResult Index()
        {
            var buses = db.BUSES.Include(b => b.COLOR).Include(b => b.MODEL);
            return View(buses.ToList());
        }

        //
        // GET: /Bus/Details/5

        //public ActionResult Details(short id = 0)
        //{
        //    Bus bus = db.BUSES.Find(id);
        //    if (bus == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bus);
        //}

        //
        // GET: /Bus/Create

        public ActionResult Create()
        {
            ViewBag.COLOR_ID = new SelectList(db.COLORS, "COLOR_ID", "COLOR_NAME");
            ViewBag.MODEL_ID = new SelectList(db.MODELS, "MODEL_ID", "MODEL_NAME");
            return View();
        }

        //
        // POST: /Bus/Create

        [HttpPost]
        public ActionResult Create(Bus bus)
        {
            if (ModelState.IsValid)
            {
                int newId = 1;
                try
                {
                    newId = db.BUSES.Max(a => a.BUS_ID) + 1;
                }
                catch
                { }
                bus.BUS_ID =(short) newId;
                db.BUSES.Add(bus);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.COLOR_ID = new SelectList(db.COLORS, "COLOR_ID", "COLOR_NAME", bus.COLOR_ID);
            ViewBag.MODEL_ID = new SelectList(db.MODELS, "MODEL_ID", "MODEL_NAME", bus.MODEL_ID);
            return View(bus);
        }

        //
        // GET: /Bus/Edit/5

        public ActionResult Edit(short id = 0)
        {
            Bus bus = db.BUSES.Find(id);
            if (bus == null)
            {
                return HttpNotFound();
            }
            ViewBag.COLOR_ID = new SelectList(db.COLORS, "COLOR_ID", "COLOR_NAME", bus.COLOR_ID);
            ViewBag.MODEL_ID = new SelectList(db.MODELS, "MODEL_ID", "MODEL_NAME", bus.MODEL_ID);
            return View(bus);
        }

        //
        // POST: /Bus/Edit/5

        [HttpPost]
        public ActionResult Edit(Bus bus)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bus).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.COLOR_ID = new SelectList(db.COLORS, "COLOR_ID", "COLOR_NAME", bus.COLOR_ID);
            ViewBag.MODEL_ID = new SelectList(db.MODELS, "MODEL_ID", "MODEL_NAME", bus.MODEL_ID);
            return View(bus);
        }

        //
        // GET: /Bus/Delete/5

        //public ActionResult Delete(short id = 0)
        //{
        //    Bus bus = db.BUSES.Find(id);
        //    if (bus == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bus);
        //}

        ////
        //// POST: /Bus/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(short id)
        //{
        //    Bus bus = db.BUSES.Find(id);
        //    db.BUSES.Remove(bus);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}