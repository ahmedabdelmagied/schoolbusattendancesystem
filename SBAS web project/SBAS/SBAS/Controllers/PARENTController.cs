﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class PARENTController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /PARENT/

        public ActionResult Index()
        {
            return View(db.PARENTS.ToList());
        }

        //
        // GET: /PARENT/Details/5

        public ActionResult Details(int id = 0)
        {
            PARENT parent = db.PARENTS.Find(id);
            if (parent == null)
            {
                return HttpNotFound();
            }
            return View(parent);
        }

        //
        // GET: /PARENT/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /PARENT/Create

        [HttpPost]
        public ActionResult Create(PARENT parent)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.PARENTS.Max(a => a.PARENT_ID) + 1;
                }
                catch
                { }
                parent.PARENT_ID = newId;
                db.PARENTS.Add(parent);
                db.USERS.Add(new USER { GROUP_ID = 3, USER_NAME = "aaaa", USER_PASSWORD = "123", PARENT_ID = newId });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(parent);
        }

        //
        // GET: /PARENT/Edit/5

        public ActionResult Edit(int id = 0)
        {
            PARENT parent = db.PARENTS.Find(id);
            if (parent == null)
            {
                return HttpNotFound();
            }
            return View(parent);
        }

        //
        // POST: /PARENT/Edit/5

        [HttpPost]
        public ActionResult Edit(PARENT parent)
        {
            if (ModelState.IsValid)
            {
                db.Entry(parent).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(parent);
        }

        //
        // GET: /PARENT/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    PARENT parent = db.PARENTS.Find(id);
        //    if (parent == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(parent);
        //}

        ////
        //// POST: /PARENT/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    PARENT parent = db.PARENTS.Find(id);
        //    db.PARENTS.Remove(parent);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}