﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class MODELController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /MODEL/

        public ActionResult Index()
        {
            return View(db.MODELS.ToList());
        }

        //
        // GET: /MODEL/Details/5

        //public ActionResult Details(decimal id = 0)
        //{
        //    MODEL model = db.MODELS.Find(id);
        //    if (model == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(model);
        //}

        //
        // GET: /MODEL/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /MODEL/Create

        [HttpPost]
        public ActionResult Create(MODEL model)
        {
            if (ModelState.IsValid)
            {
                int newId = 1;
                try
                {
                    newId = db.MODELS.Max(a => a.MODEL_ID) + 1;
                }
                catch
                { }
                model.MODEL_ID = newId;
                db.MODELS.Add(model);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(model);
        }

        //
        // GET: /MODEL/Edit/5

        public ActionResult Edit(decimal id = 0)
        {
            MODEL model = db.MODELS.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //
        // POST: /MODEL/Edit/5

        [HttpPost]
        public ActionResult Edit(MODEL model)
        {
            if (ModelState.IsValid)
            {
                db.Entry(model).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(model);
        }

        //
        // GET: /MODEL/Delete/5

        public ActionResult Delete(decimal id = 0)
        {
            MODEL model = db.MODELS.Find(id);
            if (model == null)
            {
                return HttpNotFound();
            }
            return View(model);
        }

        //
        // POST: /MODEL/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(decimal id)
        {
            MODEL model = db.MODELS.Find(id);
            db.MODELS.Remove(model);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}