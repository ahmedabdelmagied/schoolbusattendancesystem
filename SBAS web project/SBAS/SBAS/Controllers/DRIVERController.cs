﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class DRIVERController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /DRIVER/

        public ActionResult Index()
        {
            List<LicenseType> licenseTypeList = new List<LicenseType>();

            licenseTypeList.Add(new LicenseType { id = 1, typeName = " رخصة خاصة" });
            licenseTypeList.Add(new LicenseType { id = 2, typeName = "رخصة دراجة نارية" });
            licenseTypeList.Add(new LicenseType { id = 3, typeName = "رخصة أولى مهنية" });
            licenseTypeList.Add(new LicenseType { id = 4, typeName = "رخصة ثاني مهنية" });
            licenseTypeList.Add(new LicenseType { id = 5, typeName = " رخصة ثالثة مهنية" });
            licenseTypeList.Add(new LicenseType { id = 6, typeName = " رخصة عسكرية" });
            licenseTypeList.Add(new LicenseType { id = 7, typeName = " رخصة شرطية" });
            licenseTypeList.Add(new LicenseType { id = 8, typeName = " رخصة تجربة" });
            licenseTypeList.Add(new LicenseType { id = 9, typeName = " رخصة معلم" });


            ViewBag.licenseTypeList = new SelectList(licenseTypeList, "id", "typeName");
            return View(db.DRIVERS.ToList());
        }

        //
        // GET: /DRIVER/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    DRIVER driver = db.DRIVERS.Find(id);
        //    if (driver == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(driver);
        //}

        //
        // GET: /DRIVER/Create

        public ActionResult Create()
        {
            List<LicenseType> licenseTypeList = new List<LicenseType>();
            
            licenseTypeList.Add(new LicenseType { id = 1, typeName = " رخصة خاصة" });
            licenseTypeList.Add(new LicenseType { id = 2, typeName = "رخصة دراجة نارية" });
            licenseTypeList.Add(new LicenseType { id = 3, typeName = "رخصة أولى مهنية" });
            licenseTypeList.Add(new LicenseType { id = 4, typeName = "رخصة ثاني مهنية" });
            licenseTypeList.Add(new LicenseType { id = 5, typeName = " رخصة ثالثة مهنية" });
            licenseTypeList.Add(new LicenseType { id = 6, typeName = " رخصة عسكرية" });
            licenseTypeList.Add(new LicenseType { id = 7, typeName = " رخصة شرطية" });
            licenseTypeList.Add(new LicenseType { id = 8, typeName = " رخصة تجربة" });
            licenseTypeList.Add(new LicenseType { id = 9, typeName = " رخصة معلم" });
     

            ViewBag.licenseTypeList = new SelectList(licenseTypeList, "id", "typeName");

            return View();
        }
        class LicenseType{
            public int id { get; set; }
            public string typeName { get; set; }
        }

        //
        // POST: /DRIVER/Create

        [HttpPost]
        public ActionResult Create(DRIVER driver)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.DRIVERS.Max(a => a.DRIVER_ID) + 1;
                }
                catch
                { }
                driver.DRIVER_ID = newId;
                db.DRIVERS.Add(driver);
                db.USERS.Add(new USER { GROUP_ID = 2, USER_NAME = "aaaa", USER_PASSWORD = "123", DRIVER_ID = newId });
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<LicenseType> licenseTypeList = new List<LicenseType>();

            licenseTypeList.Add(new LicenseType { id = 1, typeName = " رخصة خاصة" });
            licenseTypeList.Add(new LicenseType { id = 2, typeName = "رخصة دراجة نارية" });
            licenseTypeList.Add(new LicenseType { id = 3, typeName = "رخصة أولى مهنية" });
            licenseTypeList.Add(new LicenseType { id = 4, typeName = "رخصة ثاني مهنية" });
            licenseTypeList.Add(new LicenseType { id = 5, typeName = " رخصة ثالثة مهنية" });
            licenseTypeList.Add(new LicenseType { id = 6, typeName = " رخصة عسكرية" });
            licenseTypeList.Add(new LicenseType { id = 7, typeName = " رخصة شرطية" });
            licenseTypeList.Add(new LicenseType { id = 8, typeName = " رخصة تجربة" });
            licenseTypeList.Add(new LicenseType { id = 9, typeName = " رخصة معلم" });


            ViewBag.licenseTypeList = new SelectList(licenseTypeList, "id", "typeName");
            return View(driver);
        }

        //
        // GET: /DRIVER/Edit/5

        public ActionResult Edit(int id = 0)
        {
            DRIVER driver = db.DRIVERS.Find(id);
            if (driver == null)
            {
                return HttpNotFound();
            }
            List<LicenseType> licenseTypeList = new List<LicenseType>();

            licenseTypeList.Add(new LicenseType { id = 1, typeName = " رخصة خاصة" });
            licenseTypeList.Add(new LicenseType { id = 2, typeName = "رخصة دراجة نارية" });
            licenseTypeList.Add(new LicenseType { id = 3, typeName = "رخصة أولى مهنية" });
            licenseTypeList.Add(new LicenseType { id = 4, typeName = "رخصة ثاني مهنية" });
            licenseTypeList.Add(new LicenseType { id = 5, typeName = " رخصة ثالثة مهنية" });
            licenseTypeList.Add(new LicenseType { id = 6, typeName = " رخصة عسكرية" });
            licenseTypeList.Add(new LicenseType { id = 7, typeName = " رخصة شرطية" });
            licenseTypeList.Add(new LicenseType { id = 8, typeName = " رخصة تجربة" });
            licenseTypeList.Add(new LicenseType { id = 9, typeName = " رخصة معلم" });


            ViewBag.licenseTypeList = new SelectList(licenseTypeList, "id", "typeName");
            return View(driver);
        }

        //
        // POST: /DRIVER/Edit/5

        [HttpPost]
        public ActionResult Edit(DRIVER driver)
        {
            if (ModelState.IsValid)
            {
                db.Entry(driver).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<LicenseType> licenseTypeList = new List<LicenseType>();

            licenseTypeList.Add(new LicenseType { id = 1, typeName = " رخصة خاصة" });
            licenseTypeList.Add(new LicenseType { id = 2, typeName = "رخصة دراجة نارية" });
            licenseTypeList.Add(new LicenseType { id = 3, typeName = "رخصة أولى مهنية" });
            licenseTypeList.Add(new LicenseType { id = 4, typeName = "رخصة ثاني مهنية" });
            licenseTypeList.Add(new LicenseType { id = 5, typeName = " رخصة ثالثة مهنية" });
            licenseTypeList.Add(new LicenseType { id = 6, typeName = " رخصة عسكرية" });
            licenseTypeList.Add(new LicenseType { id = 7, typeName = " رخصة شرطية" });
            licenseTypeList.Add(new LicenseType { id = 8, typeName = " رخصة تجربة" });
            licenseTypeList.Add(new LicenseType { id = 9, typeName = " رخصة معلم" });


            ViewBag.licenseTypeList = new SelectList(licenseTypeList, "id", "typeName");
            return View(driver);
        }

        //
        // GET: /DRIVER/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    DRIVER driver = db.DRIVERS.Find(id);
        //    if (driver == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(driver);
        //}

        ////
        //// POST: /DRIVER/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    DRIVER driver = db.DRIVERS.Find(id);
        //    db.DRIVERS.Remove(driver);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}