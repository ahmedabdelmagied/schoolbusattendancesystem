﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class ATTENDANTController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /ATTENDANT/

        public ActionResult Index()
        {
            return View(db.ATTENDANTS.ToList());
        }

        //
        // GET: /ATTENDANT/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    ATTENDANT attendant = db.ATTENDANTS.Find(id);
        //    if (attendant == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(attendant);
        //}

        //
        // GET: /ATTENDANT/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ATTENDANT/Create

        [HttpPost]
        public ActionResult Create(ATTENDANT attendant)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.ATTENDANTS.Max(a => a.ATTENDANT_ID)+1;
                }
                catch
                {}
                attendant.ATTENDANT_ID = newId;
                db.ATTENDANTS.Add(attendant);
                db.USERS.Add(new USER { GROUP_ID = 1, USER_NAME = "aaaa", USER_PASSWORD = "123", ATTENDANT_ID = newId });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(attendant);
        }

        //
        // GET: /ATTENDANT/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ATTENDANT attendant = db.ATTENDANTS.Find(id);
            if (attendant == null)
            {
                return HttpNotFound();
            }
            return View(attendant);
        }

        //
        // POST: /ATTENDANT/Edit/5

        [HttpPost]
        public ActionResult Edit(ATTENDANT attendant)
        {
            if (ModelState.IsValid)
            {
                db.Entry(attendant).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(attendant);
        }

        //
        // GET: /ATTENDANT/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    ATTENDANT attendant = db.ATTENDANTS.Find(id);
        //    if (attendant == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(attendant);
        //}

        ////
        //// POST: /ATTENDANT/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ATTENDANT attendant = db.ATTENDANTS.Find(id);
        //    db.ATTENDANTS.Remove(attendant);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}