﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class ROUTE_ATTENDANTSController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /ROUTE_ATTENDANTS/

        public ActionResult Index()
        {
            var route_attendants = db.ROUTE_ATTENDANTS.Include(r => r.ATTENDANT).Include(r => r.BUS_ROUTES);
            return View(route_attendants.ToList());
        }

        //
        // GET: /ROUTE_ATTENDANTS/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    ROUTE_ATTENDANTS route_attendants = db.ROUTE_ATTENDANTS.Find(id);
        //    if (route_attendants == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(route_attendants);
        //}

        //
        // GET: /ROUTE_ATTENDANTS/Create

        public ActionResult Create()
        {
            ViewBag.ATTENDANT_ID = new SelectList(db.ATTENDANTS, "ATTENDANT_ID", "ATTENDANT_NAME");
            ViewBag.BUS_ROUTE_SERIAL = new SelectList(db.BUS_ROUTES, "BUS_ROUTE_SERIAL", "BUS_ROUTE_SERIAL");
            return View();
        }

        //
        // POST: /ROUTE_ATTENDANTS/Create

        [HttpPost]
        public ActionResult Create(ROUTE_ATTENDANTS route_attendants)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.ROUTE_ATTENDANTS.Max(a => a.SERIAL) + 1;
                }
                catch
                { }
                route_attendants.SERIAL = newId;
                db.ROUTE_ATTENDANTS.Add(route_attendants);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ATTENDANT_ID = new SelectList(db.ATTENDANTS, "ATTENDANT_ID", "ATTENDANT_NAME", route_attendants.ATTENDANT_ID);
            ViewBag.BUS_ROUTE_SERIAL = new SelectList(db.BUS_ROUTES, "BUS_ROUTE_SERIAL", "BUS_ROUTE_SERIAL", route_attendants.BUS_ROUTE_SERIAL);
            return View(route_attendants);
        }

        //
        // GET: /ROUTE_ATTENDANTS/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ROUTE_ATTENDANTS route_attendants = db.ROUTE_ATTENDANTS.Find(id);
            if (route_attendants == null)
            {
                return HttpNotFound();
            }
            ViewBag.ATTENDANT_ID = new SelectList(db.ATTENDANTS, "ATTENDANT_ID", "ATTENDANT_NAME", route_attendants.ATTENDANT_ID);
            ViewBag.BUS_ROUTE_SERIAL = new SelectList(db.BUS_ROUTES, "BUS_ROUTE_SERIAL", "BUS_ROUTE_SERIAL", route_attendants.BUS_ROUTE_SERIAL);
            return View(route_attendants);
        }

        //
        // POST: /ROUTE_ATTENDANTS/Edit/5

        [HttpPost]
        public ActionResult Edit(ROUTE_ATTENDANTS route_attendants)
        {
            if (ModelState.IsValid)
            {
                db.Entry(route_attendants).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ATTENDANT_ID = new SelectList(db.ATTENDANTS, "ATTENDANT_ID", "ATTENDANT_NAME", route_attendants.ATTENDANT_ID);
            ViewBag.BUS_ROUTE_SERIAL = new SelectList(db.BUS_ROUTES, "BUS_ROUTE_SERIAL", "BUS_ROUTE_SERIAL", route_attendants.BUS_ROUTE_SERIAL);
            return View(route_attendants);
        }

        //
        // GET: /ROUTE_ATTENDANTS/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    ROUTE_ATTENDANTS route_attendants = db.ROUTE_ATTENDANTS.Find(id);
        //    if (route_attendants == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(route_attendants);
        //}

        ////
        //// POST: /ROUTE_ATTENDANTS/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ROUTE_ATTENDANTS route_attendants = db.ROUTE_ATTENDANTS.Find(id);
        //    db.ROUTE_ATTENDANTS.Remove(route_attendants);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}