﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class SCHOOLController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /SCHOOL/

        public ActionResult Index()
        {
            List<EDU_LEVEL> eduLevelList = new List<EDU_LEVEL>();

            eduLevelList.Add(new EDU_LEVEL { id = 1, eduName = " ابتدائي" });
            eduLevelList.Add(new EDU_LEVEL { id = 2, eduName = "اعدادي" });
            eduLevelList.Add(new EDU_LEVEL { id = 3, eduName = "ثانوي" });

            ViewBag.EDU_LEVEL = eduLevelList.ToArray();
            return View(db.SCHOOLS.ToList());
        }

        //
        // GET: /SCHOOL/Details/5

        //public ActionResult Details(short id = 0)
        //{
        //    SCHOOL school = db.SCHOOLS.Find(id);
        //    if (school == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(school);
        //}

        //
        // GET: /SCHOOL/Create

        public ActionResult Create()
        {
            List<EDU_LEVEL> eduLevelList = new List<EDU_LEVEL>();

            eduLevelList.Add(new EDU_LEVEL { id = 1, eduName = " ابتدائي" });
            eduLevelList.Add(new EDU_LEVEL { id = 2, eduName = "اعدادي" });
            eduLevelList.Add(new EDU_LEVEL { id = 3, eduName = "ثانوي" });

            ViewBag.EDU_LEVEL = new SelectList(eduLevelList, "id", "eduName");
            return View();
        }

        class EDU_LEVEL
        {
            public int id { get; set; }
            public string eduName { get; set; }
            public override string ToString()
            {
                return eduName;
            }
        }

        //
        // POST: /SCHOOL/Create

        [HttpPost]
        public ActionResult Create(SCHOOL school)
        {
            if (ModelState.IsValid)
            {
                int newId = 1;
                try
                {
                    newId = db.SCHOOLS.Max(a => a.SCHOOL_ID) + 1;
                }
                catch
                { }
                school.SCHOOL_ID = (short)newId;
                db.SCHOOLS.Add(school);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<EDU_LEVEL> eduLevelList = new List<EDU_LEVEL>();

            eduLevelList.Add(new EDU_LEVEL { id = 1, eduName = " ابتدائي" });
            eduLevelList.Add(new EDU_LEVEL { id = 2, eduName = "اعدادي" });
            eduLevelList.Add(new EDU_LEVEL { id = 3, eduName = "ثانوي" });

            ViewBag.EDU_LEVEL = new SelectList(eduLevelList, "id", "eduName");
            return View(school);
        }

        //
        // GET: /SCHOOL/Edit/5

        public ActionResult Edit(short id = 0)
        {
            SCHOOL school = db.SCHOOLS.Find(id);
            if (school == null)
            {
                return HttpNotFound();
            }
            List<EDU_LEVEL> eduLevelList = new List<EDU_LEVEL>();

            eduLevelList.Add(new EDU_LEVEL { id = 1, eduName = " ابتدائي" });
            eduLevelList.Add(new EDU_LEVEL { id = 2, eduName = "اعدادي" });
            eduLevelList.Add(new EDU_LEVEL { id = 3, eduName = "ثانوي" });

            ViewBag.EDU_LEVEL = new SelectList(eduLevelList, "id", "eduName");
            return View(school);
        }

        //
        // POST: /SCHOOL/Edit/5

        [HttpPost]
        public ActionResult Edit(SCHOOL school)
        {
            if (ModelState.IsValid)
            {
                db.Entry(school).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            List<EDU_LEVEL> eduLevelList = new List<EDU_LEVEL>();

            eduLevelList.Add(new EDU_LEVEL { id = 1, eduName = " ابتدائي" });
            eduLevelList.Add(new EDU_LEVEL { id = 2, eduName = "اعدادي" });
            eduLevelList.Add(new EDU_LEVEL { id = 3, eduName = "ثانوي" });

            ViewBag.EDU_LEVEL = new SelectList(eduLevelList, "id", "eduName");
            return View(school);
        }

        //
        // GET: /SCHOOL/Delete/5

        //public ActionResult Delete(short id = 0)
        //{
        //    SCHOOL school = db.SCHOOLS.Find(id);
        //    if (school == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(school);
        //}

        ////
        //// POST: /SCHOOL/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(short id)
        //{
        //    SCHOOL school = db.SCHOOLS.Find(id);
        //    db.SCHOOLS.Remove(school);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}