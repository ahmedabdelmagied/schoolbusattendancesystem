﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class EMPLOYEEController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /EMPLOYEE/

        public ActionResult Index()
        {
            return View(db.EMPLOYEES.ToList());
        }

        //
        // GET: /EMPLOYEE/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    EMPLOYEE employee = db.EMPLOYEES.Find(id);
        //    if (employee == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(employee);
        //}

        //
        // GET: /EMPLOYEE/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /EMPLOYEE/Create

        [HttpPost]
        public ActionResult Create(EMPLOYEE employee)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.EMPLOYEES.Max(a => a.EMPLOYEE_ID) + 1;
                }
                catch
                { }
                employee.EMPLOYEE_ID = newId;
                db.EMPLOYEES.Add(employee);
                db.USERS.Add(new USER { GROUP_ID = 4, USER_NAME = "aaaa", USER_PASSWORD = "123", EMPLOYEE_ID = newId });
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(employee);
        }

        //
        // GET: /EMPLOYEE/Edit/5

        public ActionResult Edit(int id = 0)
        {
            EMPLOYEE employee = db.EMPLOYEES.Find(id);
            if (employee == null)
            {
                return HttpNotFound();
            }
            return View(employee);
        }

        //
        // POST: /EMPLOYEE/Edit/5

        [HttpPost]
        public ActionResult Edit(EMPLOYEE employee)
        {
            if (ModelState.IsValid)
            {
                db.Entry(employee).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(employee);
        }

        //
        // GET: /EMPLOYEE/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    EMPLOYEE employee = db.EMPLOYEES.Find(id);
        //    if (employee == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(employee);
        //}

        ////
        //// POST: /EMPLOYEE/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    EMPLOYEE employee = db.EMPLOYEES.Find(id);
        //    db.EMPLOYEES.Remove(employee);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}