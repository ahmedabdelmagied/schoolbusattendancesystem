﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class COLORController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /COLOR/

        public ActionResult Index()
        {
            return View(db.COLORS.ToList());
        }

        //
        // GET: /COLOR/Details/5

        //public ActionResult Details(short id = 0)
        //{
        //    COLOR color = db.COLORS.Find(id);
        //    if (color == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(color);
        //}

        //
        // GET: /COLOR/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /COLOR/Create

        [HttpPost]
        public ActionResult Create(COLOR color)
        {
            if (ModelState.IsValid)
            {
                int newId = 1;
                try
                {
                    newId = db.COLORS.Max(a => a.COLOR_ID) + 1;
                }
                catch
                { }
                color.COLOR_ID = (short)newId;
                db.COLORS.Add(color);
                db.SaveChanges();
                return RedirectToAction("Index");
            }
           
            return View(color);
        }

        //
        // GET: /COLOR/Edit/5

        public ActionResult Edit(short id = 0)
        {
            COLOR color = db.COLORS.Find(id);
            if (color == null)
            {
                return HttpNotFound();
            }
            return View(color);
        }

        //
        // POST: /COLOR/Edit/5

        [HttpPost]
        public ActionResult Edit(COLOR color)
        {
            if (ModelState.IsValid)
            {
                db.Entry(color).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(color);
        }

        //
        // GET: /COLOR/Delete/5

        //public ActionResult Delete(short id = 0)
        //{
        //    COLOR color = db.COLORS.Find(id);
        //    if (color == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(color);
        //}

        ////
        //// POST: /COLOR/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(short id)
        //{
        //    COLOR color = db.COLORS.Find(id);
        //    db.COLORS.Remove(color);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}