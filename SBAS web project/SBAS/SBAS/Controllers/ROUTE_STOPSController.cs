﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class ROUTE_STOPSController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /ROUTE_STOPS/

        public ActionResult Index()
        {
            var route_stops = db.ROUTE_STOPS.Include(r => r.ROUTE).Include(r => r.STOP);
            return View(route_stops.ToList());
        }

        //
        // GET: /ROUTE_STOPS/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    ROUTE_STOPS route_stops = db.ROUTE_STOPS.Find(id);
        //    if (route_stops == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(route_stops);
        //}

        //
        // GET: /ROUTE_STOPS/Create

        public ActionResult Create()
        {
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME");
            ViewBag.STOP_ID = new SelectList(db.STOPS, "STOP_ID", "STOP_NAME");
            return View();
        }

        //
        // POST: /ROUTE_STOPS/Create

        [HttpPost]
        public ActionResult Create(ROUTE_STOPS route_stops)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.ROUTE_STOPS.Max(a => a.SERIAL) + 1;
                }
                catch
                { }
                route_stops.SERIAL = newId;
                db.ROUTE_STOPS.Add(route_stops);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME", route_stops.ROUTE_ID);
            ViewBag.STOP_ID = new SelectList(db.STOPS, "STOP_ID", "STOP_NAME", route_stops.STOP_ID);
            return View(route_stops);
        }

        //
        // GET: /ROUTE_STOPS/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ROUTE_STOPS route_stops = db.ROUTE_STOPS.Find(id);
            if (route_stops == null)
            {
                return HttpNotFound();
            }
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME", route_stops.ROUTE_ID);
            ViewBag.STOP_ID = new SelectList(db.STOPS, "STOP_ID", "STOP_NAME", route_stops.STOP_ID);
            return View(route_stops);
        }

        //
        // POST: /ROUTE_STOPS/Edit/5

        [HttpPost]
        public ActionResult Edit(ROUTE_STOPS route_stops)
        {
            if (ModelState.IsValid)
            {
                db.Entry(route_stops).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME", route_stops.ROUTE_ID);
            ViewBag.STOP_ID = new SelectList(db.STOPS, "STOP_ID", "STOP_NAME", route_stops.STOP_ID);
            return View(route_stops);
        }

        //
        // GET: /ROUTE_STOPS/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    ROUTE_STOPS route_stops = db.ROUTE_STOPS.Find(id);
        //    if (route_stops == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(route_stops);
        //}

        ////
        //// POST: /ROUTE_STOPS/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ROUTE_STOPS route_stops = db.ROUTE_STOPS.Find(id);
        //    db.ROUTE_STOPS.Remove(route_stops);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}