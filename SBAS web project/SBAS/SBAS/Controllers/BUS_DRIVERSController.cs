﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;
using SBAS.ViewModel;

namespace SBAS.Controllers
{
    public class BUS_DRIVERSController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /BUS_DRIVERS/

        public ActionResult Index()
        {
            var bus_drivers = db.BUS_DRIVERS.Include(b => b.Bus).Include(b => b.DRIVER);
            var bus_routes = db.BUS_ROUTES.Include(r => r.ROUTE);
            List<BUS_DRIVERSandBUSROUTES> modelView = new List<BUS_DRIVERSandBUSROUTES>();
            foreach (var item in bus_drivers)
            {
                BUS_ROUTES r = bus_routes.Where(a => a.BUS_ID == item.BUS_ID && a.ARRIVAL_TIME == null).SingleOrDefault();
                modelView.Add(new BUS_DRIVERSandBUSROUTES { BUS_DRIVERS = item, BUS_ROUTES = r });
            }
            return View(modelView);
        }

        //
        // GET: /BUS_DRIVERS/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    BUS_DRIVERS bus_drivers = db.BUS_DRIVERS.Find(id);
        //    if (bus_drivers == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bus_drivers);
        //}

        //
        // GET: /BUS_DRIVERS/Create

        public ActionResult Create()
        {
            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER");
            ViewBag.DRIVER_ID = new SelectList(db.DRIVERS, "DRIVER_ID", "D_NAME");
            return View();
        }

        //
        // POST: /BUS_DRIVERS/Create

        [HttpPost]
        public ActionResult Create(BUS_DRIVERS bus_drivers)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.BUS_DRIVERS.Max(a => a.SERIAL) + 1;
                }
                catch
                { }
                bus_drivers.SERIAL = newId;
                db.BUS_DRIVERS.Add(bus_drivers);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER", bus_drivers.BUS_ID);
            ViewBag.DRIVER_ID = new SelectList(db.DRIVERS, "DRIVER_ID", "D_NAME", bus_drivers.DRIVER_ID);
            return View(bus_drivers);
        }

        //
        // GET: /BUS_DRIVERS/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BUS_DRIVERS bus_drivers = db.BUS_DRIVERS.Find(id);
            if (bus_drivers == null)
            {
                return HttpNotFound();
            }
            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER", bus_drivers.BUS_ID);
            ViewBag.DRIVER_ID = new SelectList(db.DRIVERS, "DRIVER_ID", "D_NAME", bus_drivers.DRIVER_ID);
            return View(bus_drivers);
        }

        //
        // POST: /BUS_DRIVERS/Edit/5

        [HttpPost]
        public ActionResult Edit(BUS_DRIVERS bus_drivers)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bus_drivers).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER", bus_drivers.BUS_ID);
            ViewBag.DRIVER_ID = new SelectList(db.DRIVERS, "DRIVER_ID", "D_NAME", bus_drivers.DRIVER_ID);
            return View(bus_drivers);
        }

        //
        // GET: /BUS_DRIVERS/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    BUS_DRIVERS bus_drivers = db.BUS_DRIVERS.Find(id);
        //    if (bus_drivers == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bus_drivers);
        //}

        ////
        //// POST: /BUS_DRIVERS/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    BUS_DRIVERS bus_drivers = db.BUS_DRIVERS.Find(id);
        //    db.BUS_DRIVERS.Remove(bus_drivers);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}