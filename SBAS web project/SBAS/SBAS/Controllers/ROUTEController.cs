﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;
using SBAS.ViewModel;

namespace SBAS.Controllers
{
    public class ROUTEController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /ROUTE/

        public ActionResult Index()
        {

            var routes=db.ROUTES.ToList();
            List<ROUTEspecial> routes_special = new List<ROUTEspecial>();

            foreach (var item in routes)
            {
                string start = db.STOPS.Find(Int32.Parse( item.START_POINT)).STOP_NAME;
                string end = db.STOPS.Find(Int32.Parse(item.END_POINT)).STOP_NAME;
                routes_special.Add(new ROUTEspecial { ROUTE = item, start_route_name = start, end_route_name = end });
            }
            return View(routes_special);
        }



        //
        // GET: /ROUTE/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    ROUTE route = db.ROUTES.Find(id);
        //    if (route == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(route);
        //}



        class Stop
        {
            public long id { get; set; }
            public string name { get; set; }
            public override string ToString()
            {
                return name;
            }
        }
        //
        // GET: /ROUTE/Create

        public ActionResult Create()
        {
            List<Stop> stops = new List<Stop>();
            var s= db.STOPS.ToList();
            foreach (var item in s)
            {
                stops.Add(new Stop { id = item.STOP_ID, name = item.STOP_NAME });
            }
            ViewBag.stops = new SelectList(stops, "id", "name");
            return View();
        }

        //
        // POST: /ROUTE/Create

        [HttpPost]
        public ActionResult Create(ROUTE route)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.ROUTES.Max(a => a.ROUTE_ID) + 1;
                }
                catch
                { }
                route.ROUTE_ID = newId;
                db.ROUTES.Add(route);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(route);
        }


        //
        // POST: /ROUTE/Create

        //[HttpPost]
        //public ActionResult Create(ROUTEspecial route)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        long newId = 1;
        //        try
        //        {
        //            newId = db.ROUTES.Max(a => a.ROUTE_ID) + 1;
        //        }
        //        catch
        //        { }
        //        route.ROUTE.ROUTE_ID = newId;
        //   //     route.ROUTE.START_POINT=
        //        db.ROUTES.Add(route.ROUTE);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    return View(route);
        //}
        //
        // GET: /ROUTE/Edit/5

        public ActionResult Edit(int id = 0)
        {
            ROUTE route = db.ROUTES.Find(id);
            if (route == null)
            {
                return HttpNotFound();
            }

            List<Stop> stops = new List<Stop>();
            var s = db.STOPS.ToList();
            foreach (var item in s)
            {
                stops.Add(new Stop { id = item.STOP_ID, name = item.STOP_NAME });
            }
            ViewBag.stops = new SelectList(stops, "id", "name");
            
            return View(route);
        }

        //
        // POST: /ROUTE/Edit/5

        [HttpPost]
        public ActionResult Edit(ROUTE route)
        {
            if (ModelState.IsValid)
            {
                db.Entry(route).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(route);
        }

        //
        // GET: /ROUTE/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    ROUTE route = db.ROUTES.Find(id);
        //    if (route == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(route);
        //}

        ////
        //// POST: /ROUTE/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ROUTE route = db.ROUTES.Find(id);
        //    db.ROUTES.Remove(route);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}