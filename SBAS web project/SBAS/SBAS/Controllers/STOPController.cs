﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class STOPController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /STOP/

        public ActionResult Index()
        {
            return View(db.STOPS.ToList());
        }

        //
        // GET: /STOP/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    STOP stop = db.STOPS.Find(id);
        //    if (stop == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(stop);
        //}

        //
        // GET: /STOP/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /STOP/Create

        [HttpPost]
        public ActionResult Create(STOP stop)
        {



            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.STOPS.Max(a => a.STOP_ID) + 1;

                }
                catch
                { }
                stop.STOP_ID = newId;
                db.STOPS.Add(stop);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(stop);
        }

        //
        // GET: /STOP/Edit/5

        public ActionResult Edit(int id = 0)
        {
            STOP stop = db.STOPS.Find(id);
            if (stop == null)
            {
                return HttpNotFound();
            }
            return View(stop);
        }

        //
        // POST: /STOP/Edit/5

        [HttpPost]
        public ActionResult Edit(STOP stop)
        {
            if (ModelState.IsValid)
            {
                db.Entry(stop).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(stop);
        }


        ////
        //// GET: /STOP/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    STOP stop = db.STOPS.Find(id);
        //    if (stop == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(stop);
        //}

        ////
        //// POST: /STOP/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    STOP stop = db.STOPS.Find(id);
        //    db.STOPS.Remove(stop);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}



        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}