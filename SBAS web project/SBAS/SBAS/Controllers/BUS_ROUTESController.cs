﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class BUS_ROUTESController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /BUS_ROUTES/

        public ActionResult Index()
        {
            var bus_routes = db.BUS_ROUTES.Include(b => b.Bus).Include(b => b.ROUTE);
            return View(bus_routes.ToList());
        }

        //
        // GET: /BUS_ROUTES/Details/5

        //public ActionResult Details(int id = 0)
        //{
        //    BUS_ROUTES bus_routes = db.BUS_ROUTES.Find(id);
        //    if (bus_routes == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bus_routes);
        //}

        //
        // GET: /BUS_ROUTES/Create

        public ActionResult Create()
        {
            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER");
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME");
            return View();
        }

        //
        // POST: /BUS_ROUTES/Create

        [HttpPost]
        public ActionResult Create(BUS_ROUTES bus_routes)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.BUS_ROUTES.Max(a => a.BUS_ROUTE_SERIAL) + 1;
                }
                catch
                { }
                bus_routes.BUS_ROUTE_SERIAL = newId;
                db.BUS_ROUTES.Add(bus_routes);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER", bus_routes.BUS_ID);
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME", bus_routes.ROUTE_ID);
            return View(bus_routes);
        }

        //
        // GET: /BUS_ROUTES/Edit/5

        public ActionResult Edit(int id = 0)
        {
            BUS_ROUTES bus_routes = db.BUS_ROUTES.Find(id);
            if (bus_routes == null)
            {
                return HttpNotFound();
            }
            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER", bus_routes.BUS_ID);
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME", bus_routes.ROUTE_ID);
            return View(bus_routes);
        }

        //
        // POST: /BUS_ROUTES/Edit/5

        [HttpPost]
        public ActionResult Edit(BUS_ROUTES bus_routes)
        {
            if (ModelState.IsValid)
            {
                db.Entry(bus_routes).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BUS_ID = new SelectList(db.BUSES, "BUS_ID", "BUS_NUMBER", bus_routes.BUS_ID);
            ViewBag.ROUTE_ID = new SelectList(db.ROUTES, "ROUTE_ID", "ROUTE_NAME", bus_routes.ROUTE_ID);
            return View(bus_routes);
        }

        //
        // GET: /BUS_ROUTES/Delete/5

        //public ActionResult Delete(int id = 0)
        //{
        //    BUS_ROUTES bus_routes = db.BUS_ROUTES.Find(id);
        //    if (bus_routes == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(bus_routes);
        //}

        ////
        //// POST: /BUS_ROUTES/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    BUS_ROUTES bus_routes = db.BUS_ROUTES.Find(id);
        //    db.BUS_ROUTES.Remove(bus_routes);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}