﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using SBAS.Models;

namespace SBAS.Controllers
{
    public class STUDENTController : Controller
    {
        private DB_A4EA26_sbasEntities db = new DB_A4EA26_sbasEntities();

        //
        // GET: /STUDENT/

        public ActionResult Index()
        {
            var students = db.STUDENTS.Include(s => s.PARENT).Include(s => s.SCHOOL);
            return View(students.ToList());
        }

        //
        // GET: /STUDENT/Details/5

        //public ActionResult Details(long id = 0)
        //{
        //    STUDENT student = db.STUDENTS.Find(id);
        //    if (student == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(student);
        //}

        //
        // GET: /STUDENT/Create

        public ActionResult Create()
        {
            ViewBag.PARENT_ID = new SelectList(db.PARENTS, "PARENT_ID", "PARENT_NAME");
            ViewBag.SCHOOL_ID = new SelectList(db.SCHOOLS, "SCHOOL_ID", "SCHOOL_NAME");
            return View();
        }

        //
        // POST: /STUDENT/Create

        [HttpPost]
        public ActionResult Create(STUDENT student)
        {
            if (ModelState.IsValid)
            {
                long newId = 1;
                try
                {
                    newId = db.STUDENTS.Max(a => a.STUDENT_ID) + 1;
                }
                catch
                { }
                student.STUDENT_ID = newId;
                db.STUDENTS.Add(student);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.PARENT_ID = new SelectList(db.PARENTS, "PARENT_ID", "PARENT_NAME", student.PARENT_ID);
            ViewBag.SCHOOL_ID = new SelectList(db.SCHOOLS, "SCHOOL_ID", "SCHOOL_NAME", student.SCHOOL_ID);
            return View(student);
        }

        //
        // GET: /STUDENT/Edit/5

        public ActionResult Edit(long id = 0)
        {
            STUDENT student = db.STUDENTS.Find(id);
            if (student == null)
            {
                return HttpNotFound();
            }
            ViewBag.PARENT_ID = new SelectList(db.PARENTS, "PARENT_ID", "PARENT_NAME", student.PARENT_ID);
            ViewBag.SCHOOL_ID = new SelectList(db.SCHOOLS, "SCHOOL_ID", "SCHOOL_NAME", student.SCHOOL_ID);
            return View(student);
        }

        //
        // POST: /STUDENT/Edit/5

        [HttpPost]
        public ActionResult Edit(STUDENT student)
        {
            HttpPostedFileBase file = Request.Files[0];
            if (ModelState.IsValid)
            {
                db.Entry(student).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.PARENT_ID = new SelectList(db.PARENTS, "PARENT_ID", "PARENT_NAME", student.PARENT_ID);
            ViewBag.SCHOOL_ID = new SelectList(db.SCHOOLS, "SCHOOL_ID", "SCHOOL_NAME", student.SCHOOL_ID);
            return View(student);
        }

        //
        // GET: /STUDENT/Delete/5

        //public ActionResult Delete(long id = 0)
        //{
        //    STUDENT student = db.STUDENTS.Find(id);
        //    if (student == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(student);
        //}

        ////
        //// POST: /STUDENT/Delete/5

        //[HttpPost, ActionName("Delete")]
        //public ActionResult DeleteConfirmed(long id)
        //{
        //    STUDENT student = db.STUDENTS.Find(id);
        //    db.STUDENTS.Remove(student);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}