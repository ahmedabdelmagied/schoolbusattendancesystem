//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBAS.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class BUS_ROUTES
    {
        public BUS_ROUTES()
        {
            this.PICKED_UP_STUDENTS = new HashSet<PICKED_UP_STUDENTS>();
            this.ROUTE_ATTENDANTS = new HashSet<ROUTE_ATTENDANTS>();
        }
    
        public long BUS_ROUTE_SERIAL { get; set; }
        public Nullable<int> BUS_ID { get; set; }
        public Nullable<long> ROUTE_ID { get; set; }
        public Nullable<System.DateTime> START_TIME { get; set; }
        public Nullable<System.DateTime> ARRIVAL_TIME { get; set; }
    
        public virtual Bus Bus { get; set; }
        public virtual ROUTE ROUTE { get; set; }
        public virtual ICollection<PICKED_UP_STUDENTS> PICKED_UP_STUDENTS { get; set; }
        public virtual ICollection<ROUTE_ATTENDANTS> ROUTE_ATTENDANTS { get; set; }
    }
}
