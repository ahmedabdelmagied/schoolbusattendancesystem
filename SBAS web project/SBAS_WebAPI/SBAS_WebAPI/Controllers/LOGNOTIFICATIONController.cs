﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Concurrent;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using TableDependency.SqlClient;
using TableDependency.SqlClient.Base.EventArgs;

namespace SBAS_WebAPI.Controllers
{
   
    public class LOGNOTIFICATIONController : ApiController
    {
       
        private static ConcurrentDictionary<string,ConcurrentBagDriverLog> clients;
        private static readonly string _connectionString = "Data Source=SQL6007.site4now.net;Initial Catalog=DB_A4EA26_sbas;User Id=DB_A4EA26_sbas_admin;Password=sbas123456789;MultipleActiveResultSets=True;App=EntityFramework";
        private static readonly SqlTableDependency<BUS_ARRIVE_LOG> _dependency;
        static LOGNOTIFICATIONController()
        {
            clients = new ConcurrentDictionary<string, ConcurrentBagDriverLog>();

           // var mapper = new ModelToTableMapper<COLORCLASS>();

            _dependency = new SqlTableDependency<BUS_ARRIVE_LOG>(_connectionString, "BUS_ARRIVE_LOG", "dbo");
            _dependency.OnChanged += _dependency_OnChanged;
            _dependency.OnError += _dependency_OnError;
            _dependency.Start();
        }
        private static void _dependency_OnError(object sender, TableDependency.SqlClient.Base.EventArgs.ErrorEventArgs e)

        {
            throw e.Error;
        }

        private static void _dependency_OnChanged(object sender, RecordChangedEventArgs<BUS_ARRIVE_LOG> e)
        {
            if(e.ChangeType== TableDependency.SqlClient.Base.Enums.ChangeType.Insert)
            { //get model data here then send to check for the clients and send them the appropriate data
                int bus_id = e.Entity.BUS_ID.Value;
                long route_id= e.Entity.ROUTE_ID.Value;
                long stop_id = e.Entity.STOP_ID.HasValue? e.Entity.STOP_ID.Value:0;
                string arrival_time= (e.Entity.ARRIVE_TIME!=null&& e.Entity.ARRIVE_TIME.HasValue)? e.Entity.ARRIVE_TIME.Value.ToString():"";
              
               
                sendData(bus_id, route_id, stop_id, arrival_time);


            }
           
        }


        //[HttpPost]
        //public async Task PostAsync(ChatMessage m)
        //{
        //    m.dt = DateTime.Now.ToString("MM/dd/yyyy HH:mm:ss");
        //    await ChatCallbackMsg(m);
        //}


        private static async Task sendData(int bus_id, long route_id, long stop_id,string arrival_time)
        {
            //check if a client has that bus_id and route_id
            foreach (var client in clients)
            {
                try
                {
                    string start = bus_id.ToString() + ";" + route_id.ToString() + ";";
                    if (client.Key.StartsWith(start))
                    {
                        await client.Value.streamWriter.WriteAsync("{'stop_id':" + stop_id.ToString()+ ",'arrival_time':" + arrival_time+"}");
                        await client.Value.streamWriter.FlushAsync();
                        client.Value.streamWriter.Dispose();
                        ConcurrentBagDriverLog dummy;
                        clients.TryRemove(client.Key, out dummy);
                    }
                }
                catch (Exception e)
                {
                    ConcurrentBagDriverLog dummy;
                    clients.TryRemove(client.Key, out dummy);
      
                }
            }
        }
    
        [HttpGet]
        public  HttpResponseMessage Subscribe(int bus_id,int route_id,string mac,HttpRequestMessage request)
        {
            var response = request.CreateResponse();
           
            response.Content = new PushStreamContent((a, b, c) =>
            { OnStreamAvailable(a, b, c,bus_id, route_id, mac); }, "text/event-stream");
            return response;
        }

        private void OnStreamAvailable(Stream stream, HttpContent content,
            TransportContext context, int bus_id, int route_id,string mac)
        {
          
            var client = new ConcurrentBagDriverLog();
            client.streamWriter = new StreamWriter(stream);
            client.bus_id = bus_id;
            client.route_id = route_id;
            clients.TryAdd(bus_id.ToString()+";"+route_id.ToString()+";"+mac,client);
        }
    }
}
