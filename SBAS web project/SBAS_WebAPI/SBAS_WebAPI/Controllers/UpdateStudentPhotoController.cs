﻿using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class UpdateStudentPhotoController : ApiController
    {

        [AcceptVerbs("GET")]
        [HttpGet]
        public HttpResponseMessage Get(int id, string photoPath)
        {
            using (var context = new sbasEntities())
            {
                //"C:\\Users\\HP\\Desktop\\1.jpeg"
                STUDENT s = context.STUDENTS.Find(id);
                s.PHOTO =  System.IO.File.ReadAllBytes(photoPath);
                context.SaveChanges();
              return Request.CreateResponse(HttpStatusCode.OK, s.PHOTO);


            }

        }
    }
}
