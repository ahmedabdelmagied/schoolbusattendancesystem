﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class ScanDriverBusCodeController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        //add to bus_driver
        public HttpResponseMessage Post(ScanDriverBusCodeClass scanDriverBusCodeClass)
        {
            using (var context = new sbasEntities())
            {

                context.BUS_DRIVERS.Add(new BUS_DRIVERS { DRIVER_ID = scanDriverBusCodeClass.DRIVER_ID, BUS_ID = scanDriverBusCodeClass.BUS_ID, DRIVE_DATE = DateTime.Now });

                BUS_ROUTES bus_route= context.BUS_ROUTES.Where(a => a.START_TIME != null && a.ARRIVAL_TIME == null && a.BUS_ID == scanDriverBusCodeClass.BUS_ID).FirstOrDefault();

                context.SaveChanges();
                if(bus_route!=null)
                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "true", ROUTE_ID=bus_route.ROUTE_ID });
                else
                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "false" });
            }

        }
    }
}
