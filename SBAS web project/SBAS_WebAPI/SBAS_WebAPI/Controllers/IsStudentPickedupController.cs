﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class IsStudentPickedupController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        public HttpResponseMessage Post(BusRouteStudentClass busRouteStudentClass)
        {
            using (var context = new sbasEntities())
            {


                    PICKED_UP_STUDENTS pICKED_UP_STUDENTS = context.PICKED_UP_STUDENTS.Where(a => busRouteStudentClass.BUS_ROUTE_ID==a.BUS_ROUTE_SERIAL&&busRouteStudentClass.STUDENT_ID==a.STUDENT_ID && a.DROPPED_TIME == null).FirstOrDefault();
                
                
                if (pICKED_UP_STUDENTS != null)
                        return Request.CreateResponse(HttpStatusCode.OK, new { flag = true });
                else
                    return Request.CreateResponse(HttpStatusCode.OK, new { flag = false });

            }

            }

        }
    
}
