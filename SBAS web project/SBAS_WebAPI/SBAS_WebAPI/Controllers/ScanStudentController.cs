﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class ScanStudentController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        public HttpResponseMessage Post(ScanStudentClass ScanStudentClass)
        {
            
                using (var context = new sbasEntities())
                {

                    ROUTE_STUDENTS route_student = context.ROUTE_STUDENTS.Where(a => a.STUDENT_ID == ScanStudentClass.STUDENT_ID && a.ROUTE_ID == ScanStudentClass.ROUTE_ID).FirstOrDefault();

                    if (route_student == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new { success = "false", message = "There is no such student in this route" });
                    }
                    else
                    {

                        PICKED_UP_STUDENTS PICKED_UP_STUDENTS = context.PICKED_UP_STUDENTS.Where(a => a.STUDENT_ID == ScanStudentClass.STUDENT_ID && a.BUS_ROUTE_SERIAL == ScanStudentClass.BUS_ROUTE_ID && a.DROPPED_TIME == null).FirstOrDefault();
                        if (PICKED_UP_STUDENTS != null)
                            return Request.CreateResponse(HttpStatusCode.OK, new { success = "false", message = "The Student Already Pickedup", PICKED_UP_STUDENT_SERIAL = PICKED_UP_STUDENTS.PICKED_UP_STUDENT_SERIAL });
                        else
                        {
                            if (ScanStudentClass.rideFlag == true)
                            {
                                //   context.PICKED_UP_STUDENTS.Where(a =>/*a.STUDENT_ID== ScanStudentClass.STUDENT_ID&&a.BUS_ROUTE_SERIAL== ScanStudentClass.BUS_ROUTE_ID&&a.STOP_ID== route_student.STOP_ID&&*/ a.DROPPED_TIME == null)
                                PICKED_UP_STUDENTS PICKED_UP_STUDENTSAdded = context.PICKED_UP_STUDENTS.Add(new PICKED_UP_STUDENTS { STUDENT_ID = ScanStudentClass.STUDENT_ID, BUS_ROUTE_SERIAL = ScanStudentClass.BUS_ROUTE_ID, STOP_ID = route_student.STOP_ID, PICKEDUP_TIME = DateTime.Now });
                                context.SaveChanges();
                                GetStudentDetailsController getStudentDetailsController = new GetStudentDetailsController();
                                STOP stop = context.STOPS.Find(PICKED_UP_STUDENTSAdded.STOP_ID);
                                StudentDetails student = getStudentDetailsController.getStudentDetails(ScanStudentClass.STUDENT_ID);
                                if (student == null)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "false", message = "No Student With this ID" });
                                }else
                            return Request.CreateResponse(HttpStatusCode.OK, new
                                {
                                    success = "true",
                                    message = "The Student Pickedup",
                                    PICKED_UP_STUDENT_SERIAL = PICKED_UP_STUDENTSAdded.PICKED_UP_STUDENT_SERIAL,
                                    STUDENT = student,
                                    
                                    STOP_ID = stop.STOP_ID,
                                    STOP_NAME = stop.STOP_NAME,
                                    LONGITUDE = stop.LONGITUDE,
                                    LATITUDE = stop.LATITUDE

                                });
                            }
                            else
                            {
                                if (PICKED_UP_STUDENTS == null)
                                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "false", message = "The Student Doesn't pickedup!" });
                                return Request.CreateResponse(HttpStatusCode.OK, new { success = "true", message = "The Student Info", PICKED_UP_STUDENT_SERIAL = PICKED_UP_STUDENTS.PICKED_UP_STUDENT_SERIAL });
                            }
                        }


                    

                }
            }
          
        }
    }
}
