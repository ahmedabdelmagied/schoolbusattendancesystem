﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class ConfirmStopArrivalController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]

        public HttpResponseMessage Post(BusRouteStopClass confirmStopArrivalClass)
        {
            using (var context = new sbasEntities())
            {

                context.BUS_ARRIVE_LOG.Add(new BUS_ARRIVE_LOG
                {
                    ARRIVE_TIME = DateTime.Now,
                    BUS_ID = confirmStopArrivalClass.BUS_ID,
                    ROUTE_ID = confirmStopArrivalClass.ROUTE_ID,
                    STOP_ID = confirmStopArrivalClass.STOP_ID
                });
                context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { success = "true" });

          
            }

        }
    }
}
