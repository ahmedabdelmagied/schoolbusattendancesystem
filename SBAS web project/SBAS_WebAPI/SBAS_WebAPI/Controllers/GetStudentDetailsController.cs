﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class GetStudentDetailsController : ApiController
    {

        [AcceptVerbs("GET")]
        [HttpGet]
        public HttpResponseMessage Get (int id)
        {
            StudentDetails student = getStudentDetails(id);
            if (student == null)
            {
                return Request.CreateResponse(HttpStatusCode.OK, new { success = "false", message = "No Student With this ID" });
            }
            return  Request.CreateResponse(HttpStatusCode.OK, student);
        }

        public StudentDetails getStudentDetails(int studentId)
        {
            using (var context = new sbasEntities())
            {
                STUDENT student = context.STUDENTS.Find(studentId);
                if (student == null) return null;
                ROUTE_STUDENTS route_student = student.ROUTE_STUDENTS.Where(a => a.STUDENT_ID == student.STUDENT_ID).FirstOrDefault();
                if (route_student == null) return null;
                STOP stop= context.STOPS.Find(route_student.STOP_ID);
                if (stop == null) return null;
                StudentDetails studentDetails = new StudentDetails
                {
                    STUDENT_ID = student.STUDENT_ID,
                    STUDENT_NID = student.STUDENT_NID,
                    STUDENT_NAME = student.STUDENT_NAME,
                    STUDENT_MOBILE = student.STUDENT_MOBILE,
                    SCHOOL_ID = student.SCHOOL_ID,
                    PARENT_ID = student.PARENT_ID,
                    CLASS = student.CLASS,
                    PHOTO = student.PHOTO,


                    PARENT_NAME = student.PARENT.PARENT_NAME,
                    PARENT_JOB = student.PARENT.PARENT_JOB,
                    PARENT_EMAIL = student.PARENT.PARENT_EMAIL,
                    PARENT_ADDRESS = student.PARENT.PARENT_ADDRESS,


                    SCHOOL_NAME = student.SCHOOL.SCHOOL_NAME,
                    EDU_LEVEL = student.SCHOOL.EDU_LEVEL,

                    SCHOOL_LATITUDE = student.SCHOOL.LATITUDE,
                    SCHOOL_LONGITUDE = student.SCHOOL.LONGITUDE,

                    STOP_ID = stop.STOP_ID ,
                    STOP_NAME = stop.STOP_NAME,
                    LONGITUDE = stop.LONGITUDE,
                    LATITUDE = stop.LATITUDE

                };
                return studentDetails;
              
            }

        }
    }
}
