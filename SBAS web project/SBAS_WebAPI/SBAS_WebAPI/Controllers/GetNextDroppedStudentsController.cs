﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class GetNextDroppedStudentsController : ApiController
    {

        [AcceptVerbs("POST")]
        [HttpPost]

        public HttpResponseMessage Post(BusRouteStopGoingFlagClass c)
        {
           
            using (var context = new sbasEntities())
            {

                List<ROUTE_STOPS> route_stops = context.ROUTE_STOPS.Where(r => r.ROUTE_ID == c.ROUTE_ID).OrderBy(m=>m.STOP_ID).ToList();
               
                int i = 0;
              
                foreach (var item in route_stops)
                {
                    if (item.STOP_ID == c.STOP_ID)
                    {
                        break;
                    }
                    i++;
                }

                if(c.goToSchoolFlag==true)//case wana ray7
                {
                    if (i < route_stops.Count)
                    {
                        ROUTE_STOPS route_stop = route_stops.ElementAt(i + 1);

                        STOP stop = route_stop.STOP;
                    
                        long stop_id = route_stop.STOP_ID.Value;
                        string stop_name = route_stop.STOP.STOP_NAME;
                        List< ROUTE_STUDENTS> route_students=  context.ROUTE_STUDENTS.Where(r => r.ROUTE_ID == c.ROUTE_ID && r.STOP_ID == stop_id).ToList();
                        var listOfStudents = new List<Tuple<long, string>>().Select(t => new { STUDENT_ID = t.Item1, STUDENT_NAME = t.Item2 }).ToList();
                        foreach (var item in route_students)
                        {
                            listOfStudents.Add(new { STUDENT_ID = item.STUDENT.STUDENT_ID, STUDENT_NAME = item.STUDENT.STUDENT_NAME });   
                        }

                         return Request.CreateResponse(HttpStatusCode.OK, new { success = "true" , listOfStudents, stop_id= stop_id, stop_name= stop_name });
                    }
                }
                else  //case wana rag3
                {
                    if (i >0)
                    {
                        ROUTE_STOPS route_stop = route_stops.ElementAt(i - 1);

                        long stop_id = route_stop.STOP_ID.Value;
                        List<ROUTE_STUDENTS> route_students = context.ROUTE_STUDENTS.Where(r => r.ROUTE_ID == c.ROUTE_ID && r.STOP_ID == stop_id).ToList();

                        var listOfStudents = new List<Tuple<long, string>>().Select(t => new { STUDENT_ID = t.Item1, STUDENT_NAME = t.Item2 }).ToList();
                        foreach (var item in route_students)
                        {
                            listOfStudents.Add(new { STUDENT_ID = item.STUDENT.STUDENT_ID, STUDENT_NAME = item.STUDENT.STUDENT_NAME });
                        }

                        return Request.CreateResponse(HttpStatusCode.OK, new { success = "true", listOfStudents });
                    }
                }


                return Request.CreateResponse(HttpStatusCode.OK, new { success = "false" });


            }

        }
    }
}
