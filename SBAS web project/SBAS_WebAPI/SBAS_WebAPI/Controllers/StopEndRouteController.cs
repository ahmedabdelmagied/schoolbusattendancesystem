﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class StopEndRouteController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        //add to route_attendant
        public HttpResponseMessage Post(StopEndRouteClass stopEndRouteClass) // flag 
        {
            using (var context = new sbasEntities())
            {
                ROUTE_ATTENDANTS route_attendant= context.ROUTE_ATTENDANTS.Where(a => a.END_DATE == null && a.START_DATE != null && a.ATTENDANT_ID == stopEndRouteClass.ATTENDANT_ID && a.BUS_ROUTE_SERIAL == stopEndRouteClass.BUS_ROUTE_ID).FirstOrDefault();
              if(route_attendant==null)
                {
                    context.ROUTE_ATTENDANTS.Add(new ROUTE_ATTENDANTS { ATTENDANT_ID = stopEndRouteClass.ATTENDANT_ID, BUS_ROUTE_SERIAL = stopEndRouteClass.BUS_ROUTE_ID, START_DATE = DateTime.Now });
                    context.SaveChanges();
                }
               
                return Request.CreateResponse(HttpStatusCode.OK,new{ success="true"});
            }

        }
    }
}
