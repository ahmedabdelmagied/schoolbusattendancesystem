﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class IsStudentRouteController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        public HttpResponseMessage Post(BusRouteStudentClass busRouteStudentClass)
        {
            using (var context = new sbasEntities())
            {

                ROUTE_STUDENTS route_student = context.ROUTE_STUDENTS.Where(a => a.STUDENT_ID == busRouteStudentClass.STUDENT_ID && a.SERIAL == busRouteStudentClass.BUS_ROUTE_ID).FirstOrDefault();

                if (route_student == null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { flag = false });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { flag = true });
             


                }

            }

        }
    }
}
