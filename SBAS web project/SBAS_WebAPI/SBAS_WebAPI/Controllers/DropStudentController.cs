﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class DropStudentController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        public HttpResponseMessage Post(DropStudentClass dropStudentClass)
        {
            using (var context = new sbasEntities())
            {
                PICKED_UP_STUDENTS pICKED_UP_STUDENTS = context.PICKED_UP_STUDENTS.Where(a => a.PICKED_UP_STUDENT_SERIAL==dropStudentClass.PICKED_UP_STUDENT_SERIAL).FirstOrDefault();
                pICKED_UP_STUDENTS.DROPPED_TIME = DateTime.Now;
                context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK,new { message="The student has been dropped"});
             
            }

        }
        
    }
}
