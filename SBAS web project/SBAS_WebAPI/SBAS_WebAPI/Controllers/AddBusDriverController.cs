﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class AddBusDriverController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
       
        public HttpResponseMessage Post(BusDriverClass busDriverClass)
        {
            using (var context = new sbasEntities())
            {
                context.BUS_DRIVERS.Add(new BUS_DRIVERS { BUS_ID = busDriverClass.BUS_ID, DRIVER_ID = busDriverClass.Driver_ID });
                context.SaveChanges();
                return Request.CreateResponse(HttpStatusCode.OK, new { success = "true" });
            }

        }
    }
}
