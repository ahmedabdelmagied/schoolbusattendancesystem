﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class BusDetailsController : ApiController
    {
        [AcceptVerbs("GET")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            using (var context = new sbasEntities())
            {
                Bus bus = context.BUSES.Where(a => a.BUS_ID == id).FirstOrDefault();
                
                if (bus != null)
                {
                    BUS_DRIVERS bus_drivers = context.BUS_DRIVERS.Where(a => a.BUS_ID == id).FirstOrDefault();
                    BUS_ROUTES bus_routes = context.BUS_ROUTES.Where(a => a.BUS_ID == id).FirstOrDefault();
                    if (bus_drivers == null|| bus_routes==null)
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                    BUSClass busObj = new BUSClass {
                        BUS_ID = bus.BUS_ID,
                        BUS_NUMBER = bus.BUS_NUMBER,
                        COLOR_NAME = bus.COLOR.COLOR_NAME,
                        MODEL_NAME = bus.MODEL.MODEL_NAME,
                        CAPACITY = bus.CAPACITY.HasValue ? bus.CAPACITY.Value : 0,
                        BUS_DRIVER_SERIAL = bus_drivers.SERIAL.ToString(),
                        BUS_DRIVER_DRIVER_ID = bus_drivers.DRIVER_ID.HasValue? bus_drivers.DRIVER_ID.Value.ToString():"",
                        BUS_DRIVER_DRIVE_DATE= bus_drivers.DRIVE_DATE.HasValue?   bus_drivers.DRIVE_DATE.Value.ToString():"",
                        BUS_DRIVER_END_DATE= bus_drivers.END_DATE.HasValue?   bus_drivers.END_DATE.Value.ToString():"",
                        BUS_DRIVER_DRIVER_NAME=bus_drivers.DRIVER.D_NAME,
                        BUS_ROUTE_SERIAL=bus_routes.BUS_ROUTE_SERIAL.ToString(),
                        BUS_ROUT_ARRIVAL_TIME= bus_routes.ARRIVAL_TIME.HasValue?  bus_routes.ARRIVAL_TIME.Value.ToString():"",
                        BUS_ROUT_START_TIME= bus_routes.START_TIME.HasValue?  bus_routes.START_TIME.Value.ToString():"",
                        ROUTE_ID= bus_routes.ROUTE_ID.HasValue?    bus_routes.ROUTE_ID.Value.ToString():""

                    };
                return Request.CreateResponse(HttpStatusCode.OK, busObj);
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }

        }
    }
}
