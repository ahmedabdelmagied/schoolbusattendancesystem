﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class GetRouteStopsController : ApiController
    {
        [AcceptVerbs("GET")]
        [HttpGet]
        public HttpResponseMessage Get(int id)//route_id
        {
            using (var context = new sbasEntities())
            {
                List<StopClass> stopsObj = new List<StopClass>();
                var stops = context.ROUTE_STOPS.Where(a => a.ROUTE_ID == id);

                foreach (var item in stops)
                {
                    StopClass stopClass = new StopClass
                    {
                        LATITUDE = item.STOP.LATITUDE,
                        LONGITUDE = item.STOP.LONGITUDE,
                        STOP_ID = item.STOP.STOP_ID,
                        STOP_NAME = item.STOP.STOP_NAME
                    };
                    stopsObj.Add(stopClass);

                }
               
                    return Request.CreateResponse(HttpStatusCode.OK, stopsObj);
              
              
            }

        }
    }
}
