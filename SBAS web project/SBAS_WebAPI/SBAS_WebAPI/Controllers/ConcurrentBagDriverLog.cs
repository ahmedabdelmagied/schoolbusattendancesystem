﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.Controllers
{
    public class ConcurrentBagDriverLog
    {
        public StreamWriter streamWriter { get; set; }
        public int bus_id { get; set; }
        public long route_id { get; set; }

    }
}