﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class GetParentStudentsController : ApiController
    {
        [AcceptVerbs("GET")]
        [HttpGet]
        public HttpResponseMessage Get(int id)
        {
            using (var context = new sbasEntities())
            {
                PARENT parent = context.PARENTS.Find(id);
                if (parent == null)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "false", message = "No Parent With this ID" });
                }

                List<STUDENT> students = parent.STUDENTS.ToList();
                List<StudentOfParentDetailsClass> studentDetails = new List<StudentOfParentDetailsClass>();
              
                foreach (var student in students)
                {
                    ROUTE_STUDENTS route_student = student.ROUTE_STUDENTS.Where(a => a.STUDENT_ID == student.STUDENT_ID).FirstOrDefault();
                    if (route_student == null) continue;
                    STOP stop = context.STOPS.Find(route_student.STOP_ID);
                    if (stop == null) continue;

                    PICKED_UP_STUDENTS pICKED_UP_STUDENTS = context.PICKED_UP_STUDENTS.Where(a=> student.STUDENT_ID == a.STUDENT_ID && a.DROPPED_TIME == null).FirstOrDefault();
                    if (pICKED_UP_STUDENTS == null) {
                        studentDetails.Add(new StudentOfParentDetailsClass
                        {
                            STUDENT_ID = student.STUDENT_ID,
                            STUDENT_NID = student.STUDENT_NID,
                            STUDENT_NAME = student.STUDENT_NAME,
                            STUDENT_MOBILE = student.STUDENT_MOBILE,
                            SCHOOL_ID = student.SCHOOL_ID,

                            PHOTO = student.PHOTO,


                            SCHOOL_NAME = student.SCHOOL.SCHOOL_NAME,
                            EDU_LEVEL = student.SCHOOL.EDU_LEVEL,

                            SCHOOL_LATITUDE = student.SCHOOL.LATITUDE,
                            SCHOOL_LONGITUDE = student.SCHOOL.LONGITUDE,

                            STOP_ID = stop.STOP_ID,
                            STOP_NAME = stop.STOP_NAME,
                            LONGITUDE = stop.LONGITUDE,
                            LATITUDE = stop.LATITUDE,
                            is_pickedup=false,
                            is_bus_moved=false
                        });
                        continue;
                    }


                    var bus_id = pICKED_UP_STUDENTS.BUS_ROUTES.BUS_ID;
                    var route_id = pICKED_UP_STUDENTS.BUS_ROUTES.ROUTE_ID;
                    BUS_ARRIVE_LOG bus_arrival_log=context.BUS_ARRIVE_LOG.Where(a => a.ROUTE_ID == route_id && a.BUS_ID == bus_id).ToList().LastOrDefault();
                    if(bus_arrival_log==null) 
                        {
                            studentDetails.Add(new StudentOfParentDetailsClass
                            {
                                STUDENT_ID = student.STUDENT_ID,
                                STUDENT_NID = student.STUDENT_NID,
                                STUDENT_NAME = student.STUDENT_NAME,
                                STUDENT_MOBILE = student.STUDENT_MOBILE,
                                SCHOOL_ID = student.SCHOOL_ID,

                                PHOTO = student.PHOTO,


                                SCHOOL_NAME = student.SCHOOL.SCHOOL_NAME,
                                EDU_LEVEL = student.SCHOOL.EDU_LEVEL,

                                SCHOOL_LATITUDE = student.SCHOOL.LATITUDE,
                                SCHOOL_LONGITUDE = student.SCHOOL.LONGITUDE,

                                STOP_ID = stop.STOP_ID,
                                STOP_NAME = stop.STOP_NAME,
                                LONGITUDE = stop.LONGITUDE,
                                LATITUDE = stop.LATITUDE,
                                is_pickedup = true,
                                is_bus_moved = false
                            });
                            continue;
                        }
                    var last_stop_id = bus_arrival_log.STOP_ID;
                    var last_stop_name = bus_arrival_log.STOP.STOP_NAME;
                   string last_arrival_time = bus_arrival_log.ARRIVE_TIME.HasValue? bus_arrival_log.ARRIVE_TIME.Value.ToString("HH:mm") :null;


                    ROUTE_STOPS route_stop= context.ROUTE_STOPS.Where(s => s.STOP_ID == route_student.STOP_ID && s.ROUTE_ID == route_id).FirstOrDefault();
                    if (route_stop == null) continue;
                    string stop_arrival_time = route_stop.ESTIMATED_ARRIVAL_TIME.HasValue? route_stop.ESTIMATED_ARRIVAL_TIME.Value.ToString("HH:mm") :null;

                    studentDetails.Add(new StudentOfParentDetailsClass
                    {
                        STUDENT_ID = student.STUDENT_ID,
                        STUDENT_NID = student.STUDENT_NID,
                        STUDENT_NAME = student.STUDENT_NAME,
                        STUDENT_MOBILE = student.STUDENT_MOBILE,
                        SCHOOL_ID = student.SCHOOL_ID,
              
                        PHOTO = student.PHOTO,


                        SCHOOL_NAME = student.SCHOOL.SCHOOL_NAME,
                        EDU_LEVEL = student.SCHOOL.EDU_LEVEL,

                        SCHOOL_LATITUDE = student.SCHOOL.LATITUDE,
                        SCHOOL_LONGITUDE = student.SCHOOL.LONGITUDE,

                        STOP_ID = stop.STOP_ID,
                        STOP_NAME = stop.STOP_NAME,
                        LONGITUDE = stop.LONGITUDE,
                        LATITUDE = stop.LATITUDE,

                        last_stop_id = last_stop_id.Value,
                        last_stop_name= last_stop_name,
                        last_arrival_time = last_arrival_time,
                        stop_arrival_time = stop_arrival_time,

                        is_pickedup = true,
                        is_bus_moved = true
                    });
                    }



                return Request.CreateResponse(HttpStatusCode.OK, studentDetails);
            }
               
               
            }
           
        }
    }

