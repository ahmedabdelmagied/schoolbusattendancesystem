﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class EndAttendantRouteController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
       
        public HttpResponseMessage Post(EndAttendantClass endAttendantClass)
        {
            using (var context = new sbasEntities())
            {

                ROUTE_ATTENDANTS route_attendant = context.ROUTE_ATTENDANTS.Where(a => a.START_DATE != null && a.END_DATE == null && a.ATTENDANT_ID == endAttendantClass.attendant_id && a.BUS_ROUTE_SERIAL == endAttendantClass.bus_route_serial).FirstOrDefault();

             if (route_attendant != null)
                {
                    route_attendant.END_DATE = DateTime.Now;
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "true" });
                }
                else
                    return Request.CreateResponse(HttpStatusCode.OK, new { success = "false" });

            }

        }
    }
}
