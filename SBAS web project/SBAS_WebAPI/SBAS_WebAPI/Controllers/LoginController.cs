﻿
using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class LoginController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
       public HttpResponseMessage Post(USERClass userObj)
        {
            using (var context = new sbasEntities())
            {
                USER user = context.USERS.Where(u => u.USER_NAME == userObj.USER_NAME && u.USER_PASSWORD == userObj.USER_PASSWORD).FirstOrDefault();

                if(user!=null)
                {
                    ATTENDANT attendant = user.ATTENDANT;
                    DRIVER driver = user.DRIVER;
                    PARENT parent = user.PARENT;
                    if (attendant != null)
                    {
                        ROUTE_ATTENDANTS route_attendant = context.ROUTE_ATTENDANTS.Where(u => u.ATTENDANT_ID == attendant.ATTENDANT_ID && u.START_DATE != null && u.END_DATE == null).FirstOrDefault();


                        ATTENDANTClass attendantObj = new ATTENDANTClass { USER_ID = user.USER_ID, ATTENDANT_ID = attendant.ATTENDANT_ID, ATTENDANT_NAME = attendant.ATTENDANT_NAME, ATTENDANT_NID = attendant.ATTENDANT_NID.Value.ToString(), ATTENDANT_PHONE = attendant.ATTENDANT_PHONE, ATTENDANT_PHOTO = attendant.ATTENDANT_PHOTO };

                        if (route_attendant != null)
                            return Request.CreateResponse(HttpStatusCode.OK, new { attendantObj, BUS_ROUTE_SERIAL = route_attendant.BUS_ROUTE_SERIAL, BUS_ID = route_attendant.BUS_ROUTES.BUS_ID });
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, attendantObj);

                    }
                    else if (driver != null)
                    {
                        DriverClass driverObj = new DriverClass
                        {
                            USER_ID = user.USER_ID,
                            DRIVER_ID = driver.DRIVER_ID,
                            D_NAME = driver.D_NAME,
                            D_NID = driver.D_NID,
                            D_MOBILE = driver.D_MOBILE,
                            D_PHONE = driver.D_PHONE,
                            PHOTO = driver.PHOTO,
                            EXPIR_DATE = driver.EXPIR_DATE,
                            LICENSE_NUMBER = driver.LICENSE_NUMBER,
                            LICENSE_TYPE = driver.LICENSE_TYPE
                        };


                        return Request.CreateResponse(HttpStatusCode.OK, driverObj);
                    }
                    else if (parent != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, new { USER_ID = user.USER_ID, PARENT_ID =parent.PARENT_ID, PARENT_NAME=parent.PARENT_NAME, PARENT_NID=parent.PARENT_NID });
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest);
                    }
                }
               else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound);
                }
            }

        }

    }
}
