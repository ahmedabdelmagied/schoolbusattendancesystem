﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
namespace SBAS_WebAPI.Controllers
{
    public class ChangePasswordController : ApiController
    {
        [AcceptVerbs("POST")]
        [HttpPost]
        public HttpResponseMessage Post(ForgetPasswordClass forgetPasswordClass)
        {
            using (var context = new sbasEntities())
            {
                USER user = context.USERS.Find(forgetPasswordClass.USER_ID);

                if (user != null)
                {
                    user.USER_PASSWORD = forgetPasswordClass.USER_PASSWORD;
                    context.SaveChanges();
                    return Request.CreateResponse(HttpStatusCode.OK,new { success = "true" });
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.NotFound, new { success = "false" });
                }
            }

        }
    }
}
