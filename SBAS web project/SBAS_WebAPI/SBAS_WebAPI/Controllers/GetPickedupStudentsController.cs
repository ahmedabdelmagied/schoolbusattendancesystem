﻿using SBAS_WebAPI.ModelClasses;
using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class GetPickedupStudentsController : ApiController
    {
        [AcceptVerbs("GET")]
        [HttpGet]
        public HttpResponseMessage Get(int id) //bus_route_id
        {
            using (var context = new sbasEntities())
            {
                var pickedUpStudents = context.PICKED_UP_STUDENTS.Where(a => a.BUS_ROUTE_SERIAL == id&&a.DROPPED_TIME==null);
                List<PickedupStudentsClass> pickedUpStudentsReturn = new List<PickedupStudentsClass>();
               

                foreach (var item in pickedUpStudents)
                {
                    PickedupStudentsClass pickedupStudentsClass = new PickedupStudentsClass {

                        PICKED_UP_STUDENT_SERIAL = item.PICKED_UP_STUDENT_SERIAL,
                        
                        STUDENT_ID=item.STUDENT.STUDENT_ID,
                        STUDENT_NID=item.STUDENT.STUDENT_NID,
                        STUDENT_NAME = item.STUDENT.STUDENT_NAME,
                        STUDENT_MOBILE = item.STUDENT.STUDENT_MOBILE,
                        SCHOOL_ID = item.STUDENT.SCHOOL_ID,
                        PARENT_ID = item.STUDENT.PARENT_ID,
                        CLASS = item.STUDENT.CLASS,
                        PHOTO = item.STUDENT.PHOTO,

                     
                        PARENT_NAME = item.STUDENT.PARENT.PARENT_NAME,
                        PARENT_JOB = item.STUDENT.PARENT.PARENT_JOB,
                        PARENT_EMAIL = item.STUDENT.PARENT.PARENT_EMAIL,
                        PARENT_ADDRESS = item.STUDENT.PARENT.PARENT_ADDRESS,


                        SCHOOL_NAME = item.STUDENT.SCHOOL.SCHOOL_NAME,
                        EDU_LEVEL = item.STUDENT.SCHOOL.EDU_LEVEL,

                        SCHOOL_LATITUDE = item.STUDENT.SCHOOL.LATITUDE,
                        SCHOOL_LONGITUDE = item.STUDENT.SCHOOL.LONGITUDE,


                        STOP_ID = item.STOP.STOP_ID,
                        STOP_NAME = item.STOP.STOP_NAME,
                        LONGITUDE = item.STOP.LONGITUDE,
                        LATITUDE = item.STOP.LATITUDE,

                    };
                   
                         pickedUpStudentsReturn.Add(pickedupStudentsClass);
                    context.SaveChanges();
                }
                    return Request.CreateResponse(HttpStatusCode.OK, pickedUpStudentsReturn);
                
               
            }

        }
    }
}
