﻿//using SBAS_WebAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SBAS_WebAPI.Controllers
{
    public class ValuesController : ApiController
    {
        //Entities db = new Entities();
        //[HttpPost]
        //public object logIn(USER user)
        //{
        //    var currentUser = db.USERS.SingleOrDefault(u => u.USER_NAME == user.USER_NAME && u.USER_PASSWORD == user.USER_PASSWORD);
        //    if (currentUser != null)
        //    {
        //        return new {
        //            USER_ID = currentUser.USER_ID,
        //            GROUP_ID = currentUser.GROUP_ID
        //        };
        //    } else {
        //        return null;
        //    }

        //}
        // GET api/values
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}