﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class BUSClass
    {
        public int BUS_ID { get; set; }
        public String BUS_NUMBER { get; set; }
        public String COLOR_NAME { get; set; }
        public String MODEL_NAME { get; set; }
        public int CAPACITY { get; set; }
        public String BUS_DRIVER_SERIAL { get; set; }
        public String BUS_DRIVER_DRIVER_ID { get; set; }
        public String BUS_DRIVER_DRIVER_NAME { get; set; }
        public String BUS_DRIVER_DRIVE_DATE { get; set; }
        public String BUS_DRIVER_END_DATE { get; set; }
        public String BUS_ROUT_START_TIME { get; set; }
        public String BUS_ROUT_ARRIVAL_TIME { get; set; }
        public String BUS_ROUTE_SERIAL { get; set; }
        public String ROUTE_ID { get; set; }

    }
}