﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class BusRouteStopGoingFlagClass: BusRouteStopClass
    {
        public bool goToSchoolFlag { get; set; }
    }
}