﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class StopEndRouteClass
    {
        public int ATTENDANT_ID { get; set; }
        public int BUS_ROUTE_ID { get; set; }
        public bool START_FLAG { get; set; }
    }
}