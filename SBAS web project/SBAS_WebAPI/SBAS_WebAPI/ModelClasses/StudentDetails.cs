﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class StudentDetails
    {
        public long STUDENT_ID { get; set; }
        public Nullable<long> STUDENT_NID { get; set; }
        public string STUDENT_NAME { get; set; }
        public string STUDENT_MOBILE { get; set; }
        public Nullable<int> SCHOOL_ID { get; set; }
        public Nullable<long> PARENT_ID { get; set; }
        public string CLASS { get; set; }
        public byte[] PHOTO { get; set; }





        public Nullable<long> PARENT_NID { get; set; }
        public string PARENT_NAME { get; set; }
        public string PARENT_JOB { get; set; }
        public string PARENT_EMAIL { get; set; }
        public string PARENT_ADDRESS { get; set; }



        public string SCHOOL_NAME { get; set; }
        public Nullable<short> EDU_LEVEL { get; set; }
        public string SCHOOL_LONGITUDE { get; set; }
        public string SCHOOL_LATITUDE { get; set; }


        public long STOP_ID { get; set; }
        public string STOP_NAME { get; set; }
        public string LONGITUDE { get; set; }
        public string LATITUDE { get; set; }


    }
}