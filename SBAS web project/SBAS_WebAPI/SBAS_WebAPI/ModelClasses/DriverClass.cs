﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class DriverClass
    {
        public long USER_ID { get; set; }
        public long DRIVER_ID { get; set; }
        public string D_NAME { get; set; }
        public string D_PHONE { get; set; }
        public string D_MOBILE { get; set; }
        public Nullable<long> D_NID { get; set; }
        public Nullable<short> LICENSE_TYPE { get; set; }
        public string LICENSE_NUMBER { get; set; }
        public Nullable<System.DateTime> EXPIR_DATE { get; set; }
        public byte[] PHOTO { get; set; }
    }
}