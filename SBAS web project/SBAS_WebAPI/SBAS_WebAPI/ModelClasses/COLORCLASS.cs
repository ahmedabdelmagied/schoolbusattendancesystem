﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class COLORCLASS
    {
        public short COLOR_ID { get; set; }
        public string COLOR_NAME { get; set; }
    }
}