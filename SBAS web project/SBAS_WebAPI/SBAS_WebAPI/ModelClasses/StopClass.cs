﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class StopClass
    {
        public long STOP_ID { get; set; }
        public string STOP_NAME { get; set; }
        public string LONGITUDE { get; set; }
        public string LATITUDE { get; set; }
    }
}