﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class ScanDriverBusCodeClass
    {
        public int BUS_ID { get; set; }
        public int DRIVER_ID { get; set; }
    }
}