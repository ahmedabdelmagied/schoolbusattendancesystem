﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class StudentOfParentDetailsClass
    {
        public long STUDENT_ID { get; set; }
        public Nullable<long> STUDENT_NID { get; set; }
        public string STUDENT_NAME { get; set; }
        public string STUDENT_MOBILE { get; set; }
        public Nullable<int> SCHOOL_ID { get; set; }
        public byte[] PHOTO { get; set; }


        public string SCHOOL_NAME { get; set; }
        public Nullable<short> EDU_LEVEL { get; set; }
        public string SCHOOL_LONGITUDE { get; set; }
        public string SCHOOL_LATITUDE { get; set; }


        public long STOP_ID { get; set; }
        public string STOP_NAME { get; set; }
        public string LONGITUDE { get; set; }
        public string LATITUDE { get; set; }

        public long last_stop_id { get; set; }
        public string last_stop_name { get; set; }
        public string last_arrival_time { get; set; }
        public string stop_arrival_time { get; set; }
        public bool is_pickedup { get; set; }
        public bool is_bus_moved { get; set; }
    }
}