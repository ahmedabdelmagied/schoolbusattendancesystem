﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class ScanStudentClass
    {
        public int STUDENT_ID { get; set; }
        public int BUS_ROUTE_ID { get; set; }
        public int ROUTE_ID { get; set; }
        public bool rideFlag { get; set; }

    }
}