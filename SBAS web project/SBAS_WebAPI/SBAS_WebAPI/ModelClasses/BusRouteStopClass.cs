﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class BusRouteStopClass
    {
        public int BUS_ID { get; set; }
        public Int64 ROUTE_ID { get; set; }
        public Int64 STOP_ID { get; set; }
     
    }
}