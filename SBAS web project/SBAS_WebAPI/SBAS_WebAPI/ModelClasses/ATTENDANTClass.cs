﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SBAS_WebAPI.ModelClasses
{
    public class ATTENDANTClass
    {
        public long USER_ID { get; set; }
        public long ATTENDANT_ID { get; set; }
        public String ATTENDANT_NAME { get; set; }
        public String ATTENDANT_PHONE { get; set; }
        public String ATTENDANT_NID { get; set; }
        public byte[] ATTENDANT_PHOTO { get; set; }
    }
}