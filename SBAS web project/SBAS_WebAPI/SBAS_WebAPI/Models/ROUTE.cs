//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBAS_WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class ROUTE
    {
        public ROUTE()
        {
            this.BUS_ROUTES = new HashSet<BUS_ROUTES>();
            this.ROUTE_STOPS = new HashSet<ROUTE_STOPS>();
            this.ROUTE_STUDENTS = new HashSet<ROUTE_STUDENTS>();
            this.BUS_ARRIVE_LOG = new HashSet<BUS_ARRIVE_LOG>();
        }
    
        public long ROUTE_ID { get; set; }
        public string ROUTE_NAME { get; set; }
        public Nullable<System.DateTime> ESTIMATED_START_TIME { get; set; }
        public Nullable<System.DateTime> ESTIMATED_ARRIVAL_TIME { get; set; }
        public Nullable<decimal> DURATION { get; set; }
        public string START_POINT { get; set; }
        public string END_POINT { get; set; }
    
        public virtual ICollection<BUS_ROUTES> BUS_ROUTES { get; set; }
        public virtual ICollection<ROUTE_STOPS> ROUTE_STOPS { get; set; }
        public virtual ICollection<ROUTE_STUDENTS> ROUTE_STUDENTS { get; set; }
        public virtual ICollection<BUS_ARRIVE_LOG> BUS_ARRIVE_LOG { get; set; }
    }
}
