//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace SBAS_WebAPI.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class SCHOOL
    {
        public SCHOOL()
        {
            this.STUDENTS = new HashSet<STUDENT>();
        }
    
        public int SCHOOL_ID { get; set; }
        public string SCHOOL_NAME { get; set; }
        public Nullable<short> EDU_LEVEL { get; set; }
        public string LONGITUDE { get; set; }
        public string LATITUDE { get; set; }
    
        public virtual ICollection<STUDENT> STUDENTS { get; set; }
    }
}
