import 'package:flutter/material.dart';
import '../model/MenuITem.dart';

class SideItem extends StatefulWidget {
  MenuItem item;
  Icon iconItem;

  @override
  _SideItemState createState() => _SideItemState();
  SideItem({this.item , this.iconItem});
}

class _SideItemState extends State<SideItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      trailing: widget.iconItem,
      title: Text(
        widget.item.title,
        style: TextStyle(
          fontSize: 18.0,
          color: Colors.black
        ),
        textDirection: TextDirection.rtl,
      ),
      onTap: widget.item.onTap,
    );
  }
}
