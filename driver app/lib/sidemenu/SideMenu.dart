import 'dart:async';

import 'package:driverapp/model/MenuITem.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import './SideMenuHeader.dart';
import './sidemenuitem.dart';
import '../helpers/Constants.dart';
import '../model/Driver.dart';
import '../service/connectionStatusSingleton.dart';

// ignore: must_be_immutable
class SideMenu extends StatefulWidget {
  Driver currentUser;

  SideMenu({this.currentUser});

  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  StreamSubscription _connectionChangeStream;

  bool isOffline = false;

  @override
  initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
    _connectionChangeStream = connectionStatus.connectionChange.listen(connectionChanged);
    connectionChanged(connectionStatus.hasConnection);
  }

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
          color: Colors.white24,
          child: ListView(
              //sideItems
              children: <Widget>[
                MenuHeader(
                  currentUser: widget.currentUser,
                ),
                SizedBox(
                  height: 50,
                ),
                Expanded(
                    child: Divider(
                  height: 5.0,
                  color: Colors.black,
                )),
                SideItem(
                  iconItem: Icon(Icons.phone),
                  item: MenuItem(title: 'التحدث مع الاداره', onTap: () => Navigator.pop(context)),
                ),
                SizedBox(
                  height: 15,
                ),
                SideItem(
                    iconItem: Icon(Icons.lock),
                    item:
                        MenuItem(title: 'تغيير الرقم السرى', onTap: () => Navigator.pop(context))),
                SizedBox(
                  height: 15,
                ),
                SideItem(
                    iconItem: Icon(Icons.exit_to_app),
                    item: MenuItem(
                        title: 'تسجيل الخروج',
                        onTap: () async {
                          if (!isOffline) {
                            SharedPreferences prefs = await SharedPreferences.getInstance();
                            prefs.remove('attendant');
                            Navigator.of(context).pushNamedAndRemoveUntil(
                                loginPageTag, (Route<dynamic> route) => false);
                          } else
                            Toast.show('اعد الاتصال بالانترنت اولا', context,
                                gravity: Toast.BOTTOM, duration: Toast.LENGTH_LONG);
                        })),
              ])),
    );
  }
}
