import 'dart:convert';

import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../helpers/Constants.dart';
import '../model/Bus.dart';
import '../model/Driver.dart';
import '../screens/maps.dart';
import '../service/GlobalState.dart';

class ConfirmBus extends StatefulWidget {
  Driver driver;

  ConfirmBus({this.driver});

  @override
  _ConfirmBusState createState() => _ConfirmBusState(driver: driver);
}

class _ConfirmBusState extends State<ConfirmBus> {
  GlobalState _store = GlobalState.instance;
  String barcode = "";
  Driver driver;

  _ConfirmBusState({this.driver});

  Bus bus = Bus(
      routeId: '',
      routeSerial: '',
      busRouteStartTime: '',
      busDriverEndDate: '',
      busDriverDriveDate: '',
      busDriverId: '',
      busDriverSerial: '',
      capacity: 0,
      modelName: '',
      colorName: '',
      busNumber: '',
      busId: '',
      busRouteArrivalTime: '',
      busDriverName: '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        appBar:new AppBar(
          iconTheme: new IconThemeData(color: Colors.deepOrange),
          leading: Container(
            margin: EdgeInsets.only(left:5.0),
            width: 80.0,
            height: 80.0,
            decoration: new BoxDecoration(
                image: DecorationImage(
                    image: AssetImage("assets/images/logo.png")
                )
            ),
          ),
          backgroundColor: Colors.orange[300],
          centerTitle: false,

        ),
        body: Column(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
          new Container(
            height: 75,
            width: 175,
            child: RaisedButton(
              color: Colors.orangeAccent,
                padding: EdgeInsets.all(15),
                onPressed: () async {
//                  scan().then((val) async {
//                        final response = await http.get(
//                          '${baseURL}/busdetails?id=$barcode',
//                          headers: {"Content-type": "application/json"});
//                      print(response.statusCode);
//                      print(response.body);
//                      if (response.statusCode == 200) {
//                        // If the call to the server was successful, parse the JSON.
//                        print(response.body);
//                        setState(() {
//                          bus = Bus.fromJson(json.decode(response.body));
//                          _store.set('bus_id', bus.busId);
//                          _store.set('bus', bus);
//                          _store.set('route_id', bus.routeId);
//                          _store.set('bus-capacity', bus.capacity);
//                        });
//                    } else {
//                      // If that call was not successful, throw an error.
//                      Toast.show('افحص الاتوبيس الصحيح', context,
//                          duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
//                    }
//                  });
                  final response = await http.get(
                      '${baseURL}/busdetails?id=1',
                      headers: {"Content-type": "application/json"});
                  print(response.statusCode);
                  print(response.body);
                  if (response.statusCode == 200) {
                    // If the call to the server was successful, parse the JSON.
                    print(response.body);
                    bus = Bus.fromJson(json.decode(response.body));
                    SharedPreferences prefs = await SharedPreferences.getInstance();
                    prefs.setString("bus_id" , bus.busId);
                    prefs.setString("route_id" , bus.routeId);
                    setState(() {
                      _store.set('bus_id', bus.busId);
                      _store.set('bus', bus);
                      _store.set('route_id', bus.routeId);
                      _store.set('bus-capacity', bus.capacity);
                    });
                  } else {
                    // If that call was not successful, throw an error.
                    Toast.show('افحص الاتوبيس الصحيح', context,
                        duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
                  }
                },
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    Image(
                      image: AssetImage('assets/images/qr-code.png'),
                      width: 75,
                      height: 75,
                    ),
                    Text("Scan",style:TextStyle(color: Colors.black))
                  ],
                )),
//                child: RaisedButton(
////                    color: appGreyColorLight,
//                    padding: EdgeInsets.all(15),
//                    onPressed: () async {
//                      final response = await http.get(
//                          '${baseURL}/busdetails?id=1',
//                          headers: {"Content-type": "application/json"});
//                      print(response.statusCode);
//                      print(response.body);
//                      if (response.statusCode == 200) {
//                        // If the call to the server was successful, parse the JSON.
//                        print(response.body);
//                        setState(() {
//                          bus = Bus.fromJson(json.decode(response.body));
//                          _store.set('bus_id', bus.busId);
//                          _store.set('bus', bus);
//                          _store.set('route_id', bus.routeId);
//                          _store.set('bus-capacity', bus.capacity);
//                        });
//                      } else {
//                        // If that call was not successful, throw an error.
//                        throw Exception('Failed to load post');
//                      }
//                    },
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceAround,
//                      children: <Widget>[
//                        Image(
//                          image: AssetImage('assets/images/qr-code.png'),
//                          width: 75,
//                          height: 75,
//                        ),
//                        Text("Scan")
//                      ],
//                    )),
          ),
          SizedBox(
            height: 25,
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                convertToArabicNumbers(bus.busNumber),
                textDirection: TextDirection.rtl,
                style: TextStyle(color: Colors.brown[900] , fontSize: 16.0),
              ),
              Text(
                "رقم الاتوبيس : ",
                textDirection: TextDirection.rtl,
                style: TextStyle(fontWeight:FontWeight.bold,color:Colors.brown[900] ),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                bus.colorName,
                textDirection: TextDirection.rtl,
                style: TextStyle(color:Colors.brown[900]),
              ),
              Text(
                "لون الاتوبيس : ",
                textDirection: TextDirection.rtl,
                style: TextStyle(fontWeight:FontWeight.bold,color: Colors.brown[900]),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                bus.busDriverName,
                textDirection: TextDirection.rtl,
                style: TextStyle(color: Colors.brown[900]),
              ),
              Text(
                "سائق الاتوبيس : ",
                textDirection: TextDirection.rtl,
                style: TextStyle(fontWeight:FontWeight.bold,color: Colors.brown[900]),
              ),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                convertToArabicNumbers(bus.routeSerial),
                textDirection: TextDirection.rtl,
                style: TextStyle(color: Colors.brown[900] , fontSize: 17.0),
              ),
              Text(
                "رقم الخط : ",
                textDirection: TextDirection.rtl,
                style: TextStyle(fontWeight:FontWeight.bold,color: Colors.brown[900]),
              ),
            ],
          ),
          SizedBox(
            height: 15,
          ),FlatButton(
            color:Colors.orange,
            shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),

            child: Padding(
              padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
              child: Text("حفظ",
////              S.current.login,
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.w700,
                  fontSize: 15,
                ),
              ),
            ),
            onPressed: () async {
              // assign the driver to the bus..
              String url = '${baseURL}/AddBusDriver';
              var body = json.encode({
                'BUS_ID': _store.get("bus_id"),
                'Driver_ID': driver.DriverId,
              });

              var response =  await http.post(url, headers: {"Content-Type": "application/json"}, body: body);
              var responseDate = json.decode(response.body);
              print(response.body);
              print(responseDate);
              if (responseDate['success'] == 'true') {
                SharedPreferences prefs = await SharedPreferences.getInstance();
                prefs.setString('BUS_ROUTE_ID', bus.routeSerial);
                print(prefs.get('BUS_ROUTE_ID'));
                // get the route of the driver by getting the busObject in-order to get the routeId
                String routeId = _store.get("route_id");

                // get the stops of the route
                String getBusDetailsURL = '${baseURL}/GetRouteStops?id=';
                getBusDetailsURL += routeId;

                final response =
                await http.get(getBusDetailsURL, headers: {"Content-type": "application/json"});
                print(response.statusCode);
                print(response.body);
                if (response.statusCode == 200) {
                  // If the call to the server was successful, parse the JSON.
                  print(response.body);
                  List<dynamic> routeStops = json.decode(response.body);

                  // go to the map page and draw the stops for the user...
//                      Navigator.push(context, new MaterialPageRoute(builder: (BuildContext) => new Maps(stopsCoordinates: routeStops,)));
                  Navigator.push(
                      context,
                      new MaterialPageRoute(
                          builder: (BuildContext) => new Maps(
                            stopsCoordinates: routeStops,
                            driver: driver,
                          )));
                }

                print("here");
//                    Navigator.push(
////                      context,
//////                      MaterialPageRoute(builder: (context) => NearStudent()),
////                    );
              }
            },
          ),

        ]));
  }

  Future scan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        this.barcode = 'The user did not grant the camera permission!';
      } else {
        this.barcode = 'Unknown error: $e';
      }
    } on FormatException {
      this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)';
    } catch (e) {
      this.barcode = 'Unknown error: $e';
    }
  }
}
