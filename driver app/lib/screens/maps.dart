import 'dart:async';
import 'dart:convert';

import 'package:driverapp/model/Driver.dart';
import 'package:driverapp/screens/login_page.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_polyline_points/flutter_polyline_points.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:toast/toast.dart';

import '../dialogs/Dialog.dart';
import '../helpers/Constants.dart';
import '../model/station.dart';
import '../service/GlobalState.dart';
import 'mapcard.dart';

class Maps extends StatefulWidget {
  List<dynamic> stopsCoordinates = new List<dynamic>();
  Driver driver;

  Maps({this.stopsCoordinates, this.driver});

  @override
  MapsState createState() => MapsState(stopsCoordinates: stopsCoordinates);
}

// TODO  call service to get all station of current drivers
List<Station> getStopStations() {
  return [
    new Station(30.062235, 31.344900, "مكرم عبيد", "محطه مكرم عبيد", "4"),
    new Station(30.086634, 31.299716, "الخليفه المأمون", "الخليفه المأمون", "5"),
    new Station(30.083471, 31.295021, "كوبرى القبه", "محطه كوبرى القبه", "3"),
    new Station(30.064579, 31.273737, "العباسيه", "محطه العباسيه", "2"),
    new Station(30.062200, 31.248131, "رمسيس", "محطه رمسيس", "1"),
  ];
}

CameraPosition setCameraPosition(double latitude, double longitude, double zoom) {
  return CameraPosition(
    target: LatLng(latitude, longitude),
    zoom: zoom,
  );
}

class MapsState extends State<Maps> {
  GlobalState _store = GlobalState.instance;
  final Map<String, Marker> _markers = {};
  Map<PolylineId, Polyline> polylines = {};
  List<LatLng> polylineCoordinates = [];
  PolylinePoints polylinePoints = PolylinePoints();

  // added later...
  @override
  void initState() {
    super.initState();
    _getPolyline();
  }

  List<Station> stations = getStopStations();
  List<dynamic> stopsCoordinates = new List<dynamic>();

  MapsState({this.stopsCoordinates});

  BitmapDescriptor myIcon;

  Future<void> _markStationsOnGoogleMap(List<dynamic> stopsCoordinates) async {
    if (this.mounted) {
      setState(() {
//      stopsCoordinates = getStopStations();
        _markers.clear();

        //static
        for (int i = 0; i < stopsCoordinates.length; i++) {
          Station currentStation = stations.elementAt(i);

          final marker = Marker(
            onTap: () async {
              //TODO add to save
              SharedPreferences prefs = await SharedPreferences.getInstance();
              String bus_id = prefs.getString("bus_id");
              String route_id = prefs.getString("route_id");
              String stop_id = currentStation.stationId;

              String url = '${baseURL}/ConfirmStopArrival';
              var resul = await Dialogs.ConfirmDialog(
                  context: context, msg: "تأكيد وصول المحطه ${currentStation.title} ؟");
              if (resul == ConfirmAction.ACCEPT) {
                var body = json.encode({
                  "bus_id": bus_id,
                  "route_id": route_id,
                  "stop_id": stop_id,
                });

                var response =
                    await http.post(url, headers: {"Content-Type": "application/json"}, body: body);
                var responseDate = json.decode(response.body);
                Toast.show("تم تـــأكيد وصول محطه " + "${currentStation.title}", context,
                    duration: Toast.LENGTH_LONG, gravity: Toast.BOTTOM);
              }
            },
            markerId: MarkerId(currentStation.stationId),
            position: LatLng(currentStation.latitude, currentStation.langitude),
            infoWindow: InfoWindow(
              title: currentStation.title,
              snippet: currentStation.snippet,
            ),
          );

          _markers[currentStation.stationId] = marker;
        }
      });
    }
  }

  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex =
      CameraPosition(target: LatLng(30.086634, 31.299716), zoom: 12.4746, tilt: 50.0);

  @override
  Widget build(BuildContext context) {
    _createMarkerImageFromAsset("assets/images/bus-stop.png");
    return new Scaffold(
        appBar: PreferredSize(
          preferredSize: Size.fromHeight(0.0),
          child: AppBar(
            leading: Container(),
            backgroundColor: Colors.orange,
          ),
        ),
        body: Container(
            color: Colors.white70,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Expanded(
                  flex: 6,
                  child: Container(
                    height: 300.0,
                    child: GoogleMap(
                      mapType: MapType.normal,
                      initialCameraPosition: _kGooglePlex,
                      onMapCreated: (GoogleMapController controller) {
                        _markStationsOnGoogleMap(stations);
                        _currentDriverLocation(controller);
                        //   _trackDriver(controller);
                        _controller.complete(controller);
                      },
                      myLocationEnabled: true,
                      tiltGesturesEnabled: true,
                      compassEnabled: true,
                      scrollGesturesEnabled: true,
                      zoomGesturesEnabled: true,
                      markers: _markers.values.toSet(),
                      polylines: Set<Polyline>.of(polylines.values),
                    ),
                  ),
                ),
                Expanded(
                  flex: 2,
                  child: Container(
                    alignment: Alignment.topCenter,
                    child: Card(
                      elevation: 10.0,
                      color: Colors.grey[400],
                      child: Row(
//                        scrollDirection: Axis.vertical,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: MapCard(
                              onTap: () {
                                print("المحطه القادمه");
                              },
                              cardText: "المحطه القادمه",
                              cardImage: Image.asset("assets/images/bus.png"),
                            ),
                          ),
                          Container(
                            height: 35.0,
                            width: 1.0,
                            color: Colors.grey,
                          ),
                          Expanded(
                            child: MapCard(
                              onTap: () {
                                print("المحطه القادمه");
                              },
                              cardText: "التحدث مع الإداره",
                              cardImage: Image.asset("assets/images/chat.png"),
                            ),
                          ),
                          Container(
                            height: 35.0,
                            width: 1.0,
                            color: Colors.grey,
                          ),
                          Expanded(
                            child: MapCard(
                              onTap: () {
                                print("المحطه القادمه");
                              },
                              cardText: "الإبلاغ عن مشكله",
                              cardImage: Image.asset("assets/images/problem.png"),
                            ),
                          ),
                          Container(
                            height: 35.0,
                            width: 1.0,
                            color: Colors.grey,
                          ),
                          Expanded(
                            child: MapCard(
                              onTap: () {
                                Navigator.of(context).pushAndRemoveUntil(
                                    MaterialPageRoute(
                                        builder: (BuildContext context) => LoginPage()),
                                    (Route<dynamic> route) => false);
                              },
                              cardText: "تسجيل خروج",
                              cardImage: Image.asset("assets/images/logout.png"),
                            ),
                          ),
                        ],
                      ),
//                    ),
                    ),
                  ),
                ),
              ],
            )));
  }

  Future<void> _createMarkerImageFromAsset(String iconPath) async {
    ImageConfiguration configuration = createLocalImageConfiguration(context);
    await BitmapDescriptor.fromAssetImage(configuration, iconPath).then((onValue) {
      myIcon = onValue;
    });
  }

  _addPolyLine() {
    PolylineId id = PolylineId("poly");
    Polyline polyline =
        Polyline(width: 5, polylineId: id, color: Colors.red, points: polylineCoordinates);
    polylines[id] = polyline;
    if (this.mounted) {
      setState(() {});
    }
  }

  _getPolyline() async {
    PolylinePoints polylinePoints = PolylinePoints();

    List<PointLatLng> makramToKhalifa = polylinePoints.decodePolyline(
        "sonvDs`i~DNnC@mDCm@cj@hEqAcAEu@?xNdKljBwd@|FqK`HaNvOkM|Ns@nErOlW`GbQUlA}U`H}h@jCpAlk@");
    List<PointLatLng> khalifaToKobry =
        polylinePoints.decodePolyline("qhsvDef`~DJhBZ~Ah@xAtAnCjDjFhB|BtDvC");
    List<PointLatLng> kobryToAbbassia =
        polylinePoints.decodePolyline("}trvDoh_~Dry@`z@p@xAlBhApP|Ot^l_@dFzRg@P");
    List<PointLatLng> abbassiaToRamsis = polylinePoints.decodePolyline(
        "c~nvDob{}Df@Qf@xBvHh[J`DwElII?CF@F_JpOqBy@K|@z@lEIn@m@XrBbHsEjBGS[\\z[hgA");

    List<PointLatLng> result = [
      ...makramToKhalifa,
//      ...khalifaToKobry,
      ...kobryToAbbassia,
      ...abbassiaToRamsis
    ];
    if (result.isNotEmpty) {
      result.forEach((PointLatLng point) {
        polylineCoordinates.add(LatLng(point.latitude, point.longitude));
      });
    }
    _addPolyLine();
  }

  void _currentDriverLocation(GoogleMapController controller) async {
    BitmapDescriptor busIcon;
    await BitmapDescriptor.fromAssetImage(new ImageConfiguration(), "assets/images/bus.png")
        .then((onValue) {
      busIcon = onValue;
    });

    Position currentPosition =
        await Geolocator().getLastKnownPosition(desiredAccuracy: LocationAccuracy.high);
    CameraPosition currentDriverPosition = CameraPosition(
        bearing: 80,
        target: LatLng(currentPosition.latitude, currentPosition.longitude),
        tilt: 59.440717697143555,
        zoom: 18.0);

    controller.animateCamera(CameraUpdate.newCameraPosition(currentDriverPosition));
    _markers["GPSPosition"] = new Marker(
        draggable: true,
        onTap: () {},
        markerId: MarkerId("GPSPosition"),
        position: LatLng(currentPosition.latitude, currentPosition.longitude),
        infoWindow: InfoWindow(
          title: "currentPosition",
          snippet: "",
        ),
        icon: busIcon);
    if (this.mounted) {
      setState(() {});
    }
  }

  Future<void> _trackDriver(GoogleMapController controller) async {
    BitmapDescriptor busIcon;
    await BitmapDescriptor.fromAssetImage(new ImageConfiguration(), "assets/images/bus.png")
        .then((onValue) {
      busIcon = onValue;
    });

    var geolocator = Geolocator();
    var locationOptions = LocationOptions(accuracy: LocationAccuracy.high, distanceFilter: 100);

    StreamSubscription<Position> positionStream =
        geolocator.getPositionStream(locationOptions).listen((Position currentLocation) {
      CameraPosition currentDriverPosition = CameraPosition(
          bearing: 80,
          target: LatLng(currentLocation.latitude, currentLocation.longitude),
          tilt: 59.440717697143555,
          zoom: 18.0);

      controller.animateCamera(CameraUpdate.newCameraPosition(currentDriverPosition));
      setState(() {
        _markers["GPSPosition"] = new Marker(
            draggable: true,
            onTap: () {},
            markerId: MarkerId("GPSPosition"),
            position: LatLng(currentLocation.latitude, currentLocation.longitude),
            infoWindow: InfoWindow(
              title: "currentPosition",
              snippet: "",
            ),
            icon: busIcon);
      });
    });
  }
}
