import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import './LoginWidgets/login_wave_clippers.dart';
import '../control/LoginController.dart';
import '../helpers/Constants.dart';
import '../model/Bus.dart';
import '../model/Driver.dart';
import '../screens/ConfirmBus.dart';
import '../service//GlobalState.dart';
import 'ConfirmBus.dart';
import 'maps.dart';

class LoginPage extends StatefulWidget {
  static const ROUTE_NAME = "/login";

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  Bus _bus;
  GlobalState _store = GlobalState.instance;
  GlobalKey<FormState> _loginFormKey = new GlobalKey();
  FocusNode _passwordFocusNode, _loginFocusNode;
  bool _isShowPassWord = false;
  String _username = '';
  String _password = '';
  bool _isLoading = false;

  @override
  void initState() {
    super.initState();
    _passwordFocusNode = FocusNode();
    _loginFocusNode = FocusNode();
  }

  /// 点击控制密码是否显示
  void _showPassWord() {
    setState(() {
      _isShowPassWord = !_isShowPassWord;
    });
  }

  void _doLogin() async {
//    _loginFormKey.currentState.save();
//    setState(() {
//      _isLoading = true;
//    });
//    //登录前，先移除之前保存的登录信息。

    Driver driver = await Login.login(
        url: '/login', requestBody: {'USER_NAME': 'Omar Khatab', 'USER_PASSWORD': '123'});

    if (driver != null) {
      _store.set('driver', driver);
      //Navigator.push(context,new MaterialPageRoute(builder: (BuildContext) => new ConfirmBus(driver: driver,)));
      SharedPreferences prefs = await SharedPreferences.getInstance();

      String busRouteID = (prefs.getString('BUS_ROUTE_ID') ?? "");

      if (busRouteID == "") {
//                    Navigator.pushNamedAndRemoveUntil(
//                        context, confirmBusTag, ModalRoute.withName(confirmBusTag));
        Navigator.push(
            context,
            new MaterialPageRoute(
                builder: (context) => new ConfirmBus(
                      driver: driver,
                    )));
      } else {
        String getBusDetailsURL = '${baseURL}/GetRouteStops?id=';
        getBusDetailsURL += busRouteID;

        final response =
            await http.get(getBusDetailsURL, headers: {"Content-type": "application/json"});
        print(response.statusCode);
        print(response.body);
        if (response.statusCode == 200) {
          // If the call to the server was successful, parse the JSON.
          print(response.body);
          List<dynamic> routeStops = json.decode(response.body);

          // go to the map page and draw the stops for the user...
          Navigator.push(
              context,
              new MaterialPageRoute(
                  builder: (BuildContext) => new Maps(
                        stopsCoordinates: routeStops,
                        driver: driver,
                      )));
        }
      }

 // Navigator.of(context).pushNamed(confirmBusTag);
//                  if(driver.hasRoute == true){
//                    int id = driver.busId;
//                    String url  =  '${baseURL}/busdetails?id=';
//                    url += id.toString();
//
//                    final response = await http.get(
//                        url,
//                        headers: {"Content-type": "application/json"});
//                    print(response.statusCode);
//                    print(response.body);
//                    if (response.statusCode == 200) {
//                      // If the call to the server was successful, parse the JSON.
//                      print(response.body);
//
////                      Bus bus = Bus.fromJson(json.decode(response.body));
////                      _store.set('bus', bus);
////                      _store.set('bus-capacity', bus.capacity);
//                    }
//                    loadStudentsInBus();
//                    Navigator.push(context,  new MaterialPageRoute(builder: (BuildContext) => new NearStudent()));
//                  }else {
////                    Navigator.of(context).pushNamed(confirmBusTag);
//                  }
//                  _store.set('attendant', attendant);
    } else {
//                  Dialogs.showDialogBox(
//                      context: context,
//                      msg: " اسم المستخدم او كلمة المرور غير صحيحة");
    }
  }

  Widget WidgetMobile(){
    return ListView(
      children: <Widget>[
        Stack(
          children: <Widget>[
            ClipPath(
              clipper: WaveClipper2(),
              child: Container(
                child: Column(),
                width: double.infinity,
                height: 300,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange, Colors.orangeAccent])),
              ),
            ),

            ClipPath(
              clipper: WaveClipper3(),
              child: Container(
                child: Column(),
                width: double.infinity,
                height: 300,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange, Colors.orange.withOpacity(0.6)])),
              ),
            ),

            ClipPath(
              clipper: WaveClipper1(),
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 60,
                    ),
                    Image.asset(
                      'assets/images/logo.png',
                      width: 125,
                      height: 125,
                    ),
//                      Icon(
//                        Icons.developer_mode,
//                        color: Colors.white,
//                        size: 60,
//                      ),
//                      Text(
//                        "منظومة نقل الطلاب",
//                        style: TextStyle(
//                            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25),
//                      ),
                  ],
                ),
                width: double.infinity,
                height: 300,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange, Colors.orange])),
              ),
            ),
          ],
        ),

        SizedBox(
          height: 30,
        ),

        //登录Form
        Form(
          key: _loginFormKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: Material(
                  elevation: 2.0,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: TextFormField(
                      autofocus: true,
                      onEditingComplete: () =>
                          FocusScope.of(context).requestFocus(_passwordFocusNode),
                      validator: (value) {
//                          return value.isEmpty ? S.current.userNameCanNotBeEmpty : null;
                      },
                      onSaved: (value) {
                        this._username = value;
                      },
                      decoration: InputDecoration(
                          hintText: "ادخل اسم المستخدم",
                          prefixIcon: Material(
                            elevation: 0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: Icon(
                              Icons.account_circle,
                              color: Colors.orange,
                            ),
                          ),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
                    ),
                  ),
                ),
              ),

              SizedBox(
                height: 30,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: Material(
                  elevation: 2.0,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: TextFormField(
                      focusNode: _passwordFocusNode,
                      onEditingComplete: () =>
                          FocusScope.of(context).requestFocus(_loginFocusNode),
                      validator: (value) {
//                          return value.isEmpty ? S.current.passwordCanNotBeEmpty : null;
                      },
                      onSaved: (value) {
                        this._password = value;
                      },
                      obscureText: !_isShowPassWord,
                      decoration: InputDecoration(
                          hintText:"ادخل كلمه السر" ,
                          suffixIcon: IconButton(
                              icon: Icon(
                                _isShowPassWord ? Icons.visibility_off : Icons.visibility,
                                color: Colors.orange,
                              ),
                              onPressed: () => _showPassWord()),
//                            hintText: S.current.loginPasswordHint,x`
                          prefixIcon: Material(
                            elevation: 0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: Icon(
                              Icons.lock,
                              color: Colors.orange,
                            ),
                          ),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        //登录按钮及Loading的切换动画
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 300),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(
              child: child,
              scale: animation,
            );
          },
          child: _isLoading ? _buildLoginLoading() : _buildLoginButton(),
        ),
      ],
    );
  }

  /////////////////////////////////////////////////////
  // widget for tablet
  Widget WidgetTablet(){
    return ListView(
      children: <Widget>[
        Stack(
          children: <Widget>[
            ClipPath(
              clipper: WaveClipper2(),
              child: Container(
                child: Column(),
                width: double.infinity,
                height:400,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange, Colors.orangeAccent])),
              ),
            ),
            ClipPath(
              clipper: WaveClipper3(),
              child: Container(
                child: Column(),
                width: double.infinity,
                height: 410,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange, Colors.orange.withOpacity(0.6)])),
              ),
            ),
            ClipPath(
              clipper: WaveClipper1(),
              child: Container(
                child: Column(
                  children: <Widget>[
                    SizedBox(
                      height: 70,
                    ),
                    Image.asset(
                      'assets/images/logo.png',
                      width: 150,
                      height: 150,
                    ),
//                      Icon(
//                        Icons.developer_mode,
//                        color: Colors.white,
//                        size: 60,
//                      ),
//                      Text(
//                        "منظومة نقل الطلاب",
//                        style: TextStyle(
//                            color: Colors.white, fontWeight: FontWeight.bold, fontSize: 25),
//                      ),
                  ],
                ),
                width: double.infinity,
                height: 300,
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.orange, Colors.orange])),
              ),
            ),
          ],
        ),
        SizedBox(
          height: 190,
        ),
        //登录Form
        Form(
          key: _loginFormKey,
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: Material(
                  elevation: 2.0,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: TextFormField(
                      autofocus: true,
                      onEditingComplete: () =>
                          FocusScope.of(context).requestFocus(_passwordFocusNode),
                      validator: (value) {
//                          return value.isEmpty ? S.current.userNameCanNotBeEmpty : null;
                      },
                      onSaved: (value) {
                        this._username = value;
                      },
                      decoration: InputDecoration(
                          hintText: "ادخل اسم المستخدم",
                          prefixIcon: Material(
                            elevation: 0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: Icon(
                              Icons.account_circle,
                              color: Colors.orange,
                            ),
                          ),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 13)),
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 40,
              ),
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 32),
                child: Material(
                  elevation: 3.0,
                  borderRadius: BorderRadius.all(Radius.circular(30)),
                  child: Directionality(
                    textDirection: TextDirection.rtl,
                    child: TextFormField(
                      focusNode: _passwordFocusNode,
                      onEditingComplete: () =>
                          FocusScope.of(context).requestFocus(_loginFocusNode),
                      validator: (value) {
//                          return value.isEmpty ? S.current.passwordCanNotBeEmpty : null;
                      },
                      onSaved: (value) {
                        this._password = value;
                      },
                      obscureText: !_isShowPassWord,
                      decoration: InputDecoration(
                          hintText:"ادخل كلمه السر" ,

                          suffixIcon: IconButton(
                              icon: Icon(
                                _isShowPassWord ? Icons.visibility_off : Icons.visibility,
                                color: Colors.orange,
                              ),
                              onPressed: () => _showPassWord()),
//                            hintText: S.current.loginPasswordHint,x`
                          prefixIcon: Material(
                            elevation: 0,
                            borderRadius: BorderRadius.all(Radius.circular(30)),
                            child: Icon(
                              Icons.lock,
                              color: Colors.orange,
                            ),
                          ),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.symmetric(horizontal: 35, vertical: 13)),
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
        SizedBox(
          height: 30,
        ),
        //登录按钮及Loading的切换动画
        AnimatedSwitcher(
          duration: const Duration(milliseconds: 300),
          transitionBuilder: (Widget child, Animation<double> animation) {
            return ScaleTransition(
              child: child,
              scale: animation,
            );
          },
          child: _isLoading ? _buildLoginLoading() : _buildLoginButton(),
        ),
      ],
    );
  }
  /////////////////////////////////////////////////////////////////////////




  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: (isMobileLayout(context))? WidgetMobile():WidgetTablet()
    );
  }

  Widget _buildLoginLoading() {
    return CircularProgressIndicator();
  }

  Widget _buildLoginButton() {
    return Container(
      width: double.infinity,
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 32),
        child: FlatButton(
          color: Colors.orange,
          shape: new RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
          focusNode: _loginFocusNode,
          child: Padding(
            padding: const EdgeInsets.only(top: 12.0, bottom: 12.0),
            child: Text(
              "تسجيل الدخول",
////              S.current.login,
              style: TextStyle(
                color: Colors.white,
                fontWeight: FontWeight.w700,
                fontSize: 20,
              ),
            ),
          ),
          onPressed: () {
//            if (_loginFormKey.currentState.validate()) {
            _doLogin();
//            }
          },
        ),
      ),
    );
  }

  @override
  void dispose() {
    _passwordFocusNode.dispose();
    _loginFocusNode.dispose();
    super.dispose();
  }
}
