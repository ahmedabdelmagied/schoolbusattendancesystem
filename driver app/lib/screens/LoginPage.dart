import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../control/LoginController.dart';
import '../helpers/Constants.dart';
import '../model/Bus.dart';
import '../model/Driver.dart';
import '../screens/ConfirmBus.dart';
import '../service/GlobalState.dart';
import 'ConfirmBus.dart';
import 'maps.dart';

// 1
class LoginPage extends StatelessWidget {
  // 2
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  Bus _bus;
  GlobalState _store = GlobalState.instance;

//  Dialogs _dialog;
//  List<Student> students = [];
//
//  int availabileCapacity = 0;
//  int pickedUpNumber = 0;
//
//
//  loadStudentsInBus() async {
//    final response = await http.get('${baseURL}/getpickedupstudents/1',
//        headers: {"Content-type": "application/json"});
//    var responseData = json.decode(response.body);
//    for (var studentJson in responseData) {
//      Student student = Student.fromJson(studentJson);
//      this.students.add(student);
//    }
//    int tempCapacity = _store.get('bus-capacity');
//    tempCapacity -= Student.studentsList.length;
//    this.availabileCapacity = tempCapacity;
//    this.pickedUpNumber = _store.get('bus').capacity - availabileCapacity;
//    _store.set('bus-capacity', tempCapacity);
//  }
//

  @override
  Widget build(BuildContext context) {
    // 3a
    final logo = CircleAvatar(
      backgroundColor: Colors.transparent,
      radius: bigRadius,
      child: appLogo,
    );

    // 3b
    final username = Directionality(
        textDirection: TextDirection.rtl,
        child: TextFormField(
          controller: _usernameController,
          maxLength: 50,
          maxLines: 1,
          autofocus: true,
          decoration: InputDecoration(
              hintText: usernameHintText,
              contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(32.0),
              ),
              hintStyle: TextStyle(color: Colors.white)),
          style: TextStyle(
            color: Colors.white,
          ),
        ));

    final password = Directionality(
      textDirection: TextDirection.rtl,
      child: TextFormField(
        controller: _passwordController,
        maxLength: 16,
        maxLines: 1,
        obscureText: true,
        autofocus: true,
        decoration: InputDecoration(
            hintText: passwordHintText,
            contentPadding: EdgeInsets.fromLTRB(20.0, 10.0, 20.0, 10.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(32.0),
            ),
            hintStyle: TextStyle(color: Colors.white)),
        style: TextStyle(
          color: Colors.white,
        ),
      ),
    );

    // 3c
    final loginButton = Padding(
      padding: EdgeInsets.symmetric(vertical: 16.0),
      child: RaisedButton(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(24),
        ),
        onPressed: () async {
          String username = _usernameController.text;
          String password = _passwordController.text;
          Driver driver = await Login.login(
              url: '/login', requestBody: {'USER_NAME': username, 'USER_PASSWORD': password});
          if (driver != null) {
            _store.set('driver', driver);
            SharedPreferences prefs = await SharedPreferences.getInstance();

            String busRouteID = (prefs.getString('BUS_ROUTE_ID') ?? "");

            if (busRouteID == "") {
              Navigator.pushNamedAndRemoveUntil(
                  context, confirmBusTag, ModalRoute.withName(confirmBusTag));
            } else {
              String busString = (prefs.getString('bus') ?? "");
              if (busString == "") {
                Navigator.pushNamedAndRemoveUntil(
                    context, confirmBusTag, ModalRoute.withName(confirmBusTag));
              } else {
                _bus = Bus.fromJson(json.decode(busString));
                _store.set('bus', _bus);
                Navigator.of(context)
                    .pushNamedAndRemoveUntil(confirmBusTag, (Route<dynamic> route) => false);
              }
            }
            Navigator.of(context).pushNamed(confirmBusTag);
          } else {
//            Dialogs.showDialogBox(
//                context: context,
//                msg: " اسم المستخدم او كلمة المرور غير صحيحة");
          }
        },
        padding: EdgeInsets.all(12),
        color: appGreyColor,
        child: Text(loginButtonText, style: TextStyle(color: Colors.white)),
      ),
    );

    // 3d
    return Scaffold(
      backgroundColor: appDarkGreyColor,
      //لTODO don't forget to remove the drawer from here..
      endDrawer: Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
//            DrawerHeader(
//              child: Text('Drawer Header'),
//              decoration: BoxDecoration(
//                color: Colors.blue,
//              ),
//            ),
            ListTile(
              title: Text(
                'التحدث مع الاداره',
                style: TextStyle(
                  fontSize: 20.0,
                ),
                textDirection: TextDirection.rtl,
              ),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('التحدث مع ولى الامر',
                  style: TextStyle(fontSize: 20.0), textDirection: TextDirection.rtl),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('تغير الرقم السرى',
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                  textDirection: TextDirection.rtl),
              onTap: () {
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('وصول الخط',
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                  textDirection: TextDirection.rtl),
              onTap: () {
//                Navigator.push(context, new MaterialPageRoute(builder: (BuildContext) => new ArriveBus()));
//                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('تسجيل الخروج',
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                  textDirection: TextDirection.rtl),
              onTap: () {
                Navigator.push(
                    context, new MaterialPageRoute(builder: (BuildContext) => new LoginPage()));
//                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Center(
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.only(left: 24.0, right: 24.0),
          children: <Widget>[
            //  logo,
            SizedBox(height: bigRadius),
            username,
            password,
            SizedBox(height: buttonHeight),
            loginButton,
            RaisedButton(
              onPressed: () async {
                Driver driver = await Login.login(
                    url: '/login',
                    requestBody: {'USER_NAME': 'Omar Khatab', 'USER_PASSWORD': '123'});

                if (driver != null) {
                  _store.set('driver', driver);
                  //Navigator.push(context,new MaterialPageRoute(builder: (BuildContext) => new ConfirmBus(driver: driver,)));
                  SharedPreferences prefs = await SharedPreferences.getInstance();

                  String busRouteID = (prefs.getString('BUS_ROUTE_ID') ?? "");

                  if (busRouteID == "") {
//                    Navigator.pushNamedAndRemoveUntil(
//                        context, confirmBusTag, ModalRoute.withName(confirmBusTag));
                    Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new ConfirmBus(
                                  driver: driver,
                                )));
                  } else {
                    String getBusDetailsURL = '${baseURL}/GetRouteStops?id=';
                    getBusDetailsURL += busRouteID;

                    final response = await http
                        .get(getBusDetailsURL, headers: {"Content-type": "application/json"});
                    print(response.statusCode);
                    print(response.body);
                    if (response.statusCode == 200) {
                      // If the call to the server was successful, parse the JSON.
                      print(response.body);
                      List<dynamic> routeStops = json.decode(response.body);

                      // go to the map page and draw the stops for the user...
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (BuildContext) => new Maps(
                                    stopsCoordinates: routeStops,
                                    driver: driver,
                                  )));
                    }
                  }
                  // Navigator.of(context).pushNamed(confirmBusTag);
//                  if(driver.hasRoute == true){
//                    int id = driver.busId;
//                    String url  =  '${baseURL}/busdetails?id=';
//                    url += id.toString();
//
//                    final response = await http.get(
//                        url,
//                        headers: {"Content-type": "application/json"});
//                    print(response.statusCode);
//                    print(response.body);
//                    if (response.statusCode == 200) {
//                      // If the call to the server was successful, parse the JSON.
//                      print(response.body);
//
////                      Bus bus = Bus.fromJson(json.decode(response.body));
////                      _store.set('bus', bus);
////                      _store.set('bus-capacity', bus.capacity);
//                    }
//                    loadStudentsInBus();
//                    Navigator.push(context,  new MaterialPageRoute(builder: (BuildContext) => new NearStudent()));
//                  }else {
////                    Navigator.of(context).pushNamed(confirmBusTag);
//                  }
//                  _store.set('attendant', attendant);
                } else {
//                  Dialogs.showDialogBox(
//                      context: context,
//                      msg: " اسم المستخدم او كلمة المرور غير صحيحة");
                }
              },
              child: Text('تخطى الدخول'),
            )
          ],
        ),
      ),
    );
  }
}
