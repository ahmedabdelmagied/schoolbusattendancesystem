class Driver {
  final int DriverId;
  final String DriverName;
  final String DriverPhone;
  final String DriverNID;
  final String DriverMobile;
  final int LicenseType;
  final String LicenseNumber;
  final String DriverPhotoURL;
  final String ExpireDate;

  Driver(
    {
      this.DriverId,
      this.DriverName,
      this.DriverPhone,
      this.DriverNID,
      this.DriverMobile,
      this.LicenseType,
      this.LicenseNumber,
      this.DriverPhotoURL,
      this.ExpireDate
    });

  factory Driver.fromJson(Map<String, dynamic> json) {

    return Driver(
        DriverId: json["DRIVER_ID"],
        DriverName: json["D_NAME"],
        DriverPhone: json["D_PHONE"],
        DriverNID: json["D_MOBILE"],
        DriverMobile: '${json["D_NID"]}',
        LicenseType: json["LICENSE_TYPE"],
        LicenseNumber: json["LICENSE_NUMBER"],
        DriverPhotoURL: json["EXPIR_DATE"],
        ExpireDate: json["PHOTO"]
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["DRIVER_ID"] = DriverId;
    map["D_NAME"] = DriverName;
    map["D_PHONE"] = DriverPhone;
    map["D_NID"] = DriverNID;
    map["D_MOBILE"] = DriverMobile;
    map["LICENSE_TYPE"] = LicenseType;
    map["LICENSE_NUMBER"] = LicenseNumber;
    map["EXPIR_DATE"] = ExpireDate;
    map["PHOTO"] = DriverPhotoURL;
    return map;
  }
}




























