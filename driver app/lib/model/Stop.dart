class Stop{
  String latitude , langitude;
  String stopName , stopId;

  Stop({stopId,this.latitude, this.langitude, this.stopName});

  factory Stop.fromJson(Map<String, dynamic> json) {
    return Stop(
       latitude: json['LATITUDE'].toString(),
       langitude: json['LONGITUDE'].toString(),
       stopName: json['STOP_NAME'].toString(),
       stopId: json['STOP_ID']
    );
  }
}