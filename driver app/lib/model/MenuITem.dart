import 'package:flutter/material.dart';
class MenuItem{
  String title;
  GestureTapCallback onTap;

  MenuItem({this.title, this.onTap});

}