class Student {
  final int Student_EduLevel;
  final String Student_Name;
  final String Student_lastStopName;
  final String Student_schoolName;
  final String Student_photo;
  final String Student_stopName;
  final String Student_lastArrivalTime;
  final String Student_StopArrivalTime;
  final bool Student_isPickedup;
  final bool Student_isBusMoved;

  Student(
      {this.Student_EduLevel,
      this.Student_Name,
      this.Student_stopName,
      this.Student_lastStopName,
      this.Student_schoolName,
      this.Student_photo,
      this.Student_lastArrivalTime,
      this.Student_StopArrivalTime,
      this.Student_isPickedup,
      this.Student_isBusMoved});

  factory Student.fromJson(Map<String, dynamic> json) {
    return Student(
        Student_EduLevel: json["EDU_LEVEL"],
        Student_Name: json["STUDENT_NAME"],
        Student_lastStopName: '${json["last_stop_name"]}',
        Student_schoolName: '${json["SCHOOL_NAME"]}',
        Student_photo: json["PHOTO"],
        Student_stopName: json["STOP_NAME"],
        Student_lastArrivalTime: '${json["last_arrival_time"]}',
        Student_StopArrivalTime: '${json["stop_arrival_time"]}',
        Student_isPickedup: json["is_pickedup"],
        Student_isBusMoved: json["is_bus_moved"]);
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["EDU_LEVEL"] = this.Student_EduLevel;
    map["STUDENT_NAME"] = this.Student_Name;
    map["last_stop_name"] = this.Student_lastStopName;
    map["SCHOOL_NAME"] = this.Student_schoolName;
    map["PHOTO"] = this.Student_photo;
    map["STOP_NAME"] = this.Student_stopName;
    map["last_arrival_time"] = this.Student_lastArrivalTime;
    map["stop_arrival_time"] = this.Student_StopArrivalTime;
    map["is_pickedup"] = this.Student_isPickedup;
    map["is_bus_moved"] = this.Student_isBusMoved;

    return map;
  }

  String toJsonString() {
    return '{"EDU_LEVEL":${this.Student_EduLevel},'
        '"STUDENT_NAME":"${this.Student_Name}",'
        '"last_stop_name":"${this.Student_lastStopName}",'
        '"SCHOOL_NAME":"${this.Student_schoolName}",'
        '"PHOTO":"${this.Student_photo}",'
        '"STOP_NAME":${this.Student_stopName},'
        '"last_arrival_time":"${this.Student_lastArrivalTime}",'
        '"stop_arrival_time":"${this.Student_StopArrivalTime}",'
        '"is_pickedup":"${this.Student_isPickedup}",'
        '"is_bus_moved":"${this.Student_isBusMoved}"}';
  }
}
