import 'package:flutter/material.dart';

class DashboardDialog extends StatefulWidget {
  @override
  _DashboardDialogState createState() => _DashboardDialogState();
}

class _DashboardDialogState extends State<DashboardDialog> {
  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(66.0),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: dialogContent(context),
    );
  }

  dialogContent(BuildContext context) {
       return Container(
         child: Column(
           children: <Widget>[
             Text("first element in container"),
             Text("second element in container")
           ],
         ),
       );
  }
}
