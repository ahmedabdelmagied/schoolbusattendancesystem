import 'package:flutter/material.dart';


class MapCard extends StatelessWidget {
  final GestureTapCallback onTap;
  final String cardText;
  final Image cardImage;

  const MapCard({Key key, this.onTap, this.cardImage, this.cardText}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double size = 50.0;

        return new InkResponse(
          onTap: onTap,
          child: Container
            (
            alignment: Alignment.center,
             margin: EdgeInsets.only(left:5.0,right: 5.0,top: 25.0),
            child:Column(
            children: <Widget>[
              Expanded(child:new Container(
//               padding:EdgeInsets.all(5.0),
                width: size,
                height: size,
                decoration: new BoxDecoration(
                  color: Colors.white,
                  shape: BoxShape.circle,
                ),
                child: cardImage,
              ),),
              SizedBox(height: 5.0,),
              new Expanded(child: Container(
                child: Text(cardText , style: TextStyle(color:Colors.brown[900],fontSize: 13.0 ,fontWeight: FontWeight.bold ),),)
              )
            ],
          ),)
      );
  }
}