import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'package:http/http.dart' as http;
import 'package:parentapp/sidemenu/SideMenu.dart';
import 'package:toast/toast.dart';

import '../../helpers/Constants.dart';
import '../../model/student.dart';
import 'parentWidgets/studentCard.dart';

class ParentScreen extends StatefulWidget {
  int parentId;

  ParentScreen({this.parentId});

  @override
  _ParentScreenState createState() => _ParentScreenState();
}

class _ParentScreenState extends State<ParentScreen> {
  List<Widget> studentsWidget = new List<Widget>();

  @override
  void initState() {
    adjustStudentsCard();
    super.initState();
  }

  Future<List<Student>> getStudentsOfParent(int parentId) async {
    try {
      final response = await http
          .get('$baseURL/getParentStudents?id=${parentId}', // '$baseURL/busdetails?id=$busId'
              headers: {"Content-type": "application/json"});

      List<dynamic> students = json.decode(response.body);
      List<Student> parentStudents = new List<Student>();
      for (int i = 0; i < students.length; i++) {
        parentStudents.add(Student.fromJson(students[i]));
      }
      if (response.statusCode == 200) {
        var result = json.decode(response.body);
        print(result);
        return parentStudents;
      }
    } catch (Exception) {
      Toast.show("هناك خطأ فى الاتصال بالسيرفر", context, duration: Toast.LENGTH_LONG);
    }
    return List<Student>();
  }

  void adjustStudentsCard() async {
    List<Student> parentStudents = await getStudentsOfParent(3);
    String value = "  متوقع وصول الطالب الساعه  ${convertToArabicNumbers("6:30")} صباحا";
    studentsWidget.clear();
    for (int i = 0; i < parentStudents.length; i++) {
      Student currentStudent = parentStudents[i];
      studentsWidget.add(studentCard(
        state: value,
        latestStation: currentStudent.Student_lastStopName,
        latestStationArrival: currentStudent.Student_lastArrivalTime,
        studentName: currentStudent.Student_Name,
        schoolName: currentStudent.Student_schoolName,
        studentPhoto: currentStudent.Student_photo,
      ));
    }
    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(

        endDrawer: SideMenu(),
        appBar: new AppBar(
          iconTheme: new IconThemeData(color: Colors.white),
          leading: Container(
            margin: EdgeInsets.only(left: 5.0),
            width: 80.0,
            height: 80.0,
            decoration: new BoxDecoration(
                image: DecorationImage(image: AssetImage("assets/images/logo.png"))),
          ),
          backgroundColor: Colors.orange,
          centerTitle: false,
        ),
//      body: new ListView(
//        children: <Widget>[
//
//          Column(children: studentsWidget)
////          for(var student in parentStudents) studentCard()
//        ],
//      ),
        body: Container(
          alignment: Alignment.center,
          decoration: BoxDecoration(
            gradient: LinearGradient(
              // Where the linear gradient begins and ends
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              // Add one stop for each color. Stops should increase from 0 to 1
              stops: [0.2, 0.6, 0.8, 0.9],
              colors: [
                // Colors are easy thanks to Flutter's Colors class.
                Colors.orangeAccent,
                Colors.orange,
                Colors.orangeAccent,
                Colors.orangeAccent,
              ],
            ),
          ),
            padding: EdgeInsets.only(
                top: MediaQuery.of(context).orientation == Orientation.portrait ? 54 : 0),

            child: ListView(
              children: <Widget>[
                Container(

                    height: MediaQuery.of(context).orientation == Orientation.portrait
                        ? MediaQuery.of(context).size.height - 200
                        : MediaQuery.of(context).size.width - 300,
                    child: new Swiper(

                      itemBuilder: (BuildContext context, int index) {
                        if (index == 0)
                          return studentCard(
                            state: "",
                            latestStation: "محطه السكه الحديد",
                            latestStationArrival: "",
                            studentName: "",
                            schoolName: "",
                          );
                        if (index == 1) {
                          return studentCard(
                            state: "3",
                            latestStation: "محطه العباسيه",
                            latestStationArrival: "ddff",
                            studentName: "adfe",
                            schoolName: "adsf",
                          );
                        }
                        if (index == 2) {
                          return studentCard(
                            state: "3",
                            latestStation: "محطه العباسيه",
                            latestStationArrival: "12 و نص صباحا",
                            studentName: "احمد عبد المجيد",
                            schoolName: "مدرسه النهضه التجريبيه",
                          );
                        }
                      },
                      itemCount: 3,
                      pagination: new SwiperPagination(),
                      control: new SwiperControl(),

                    )),
              ],
            )));
  }
}
