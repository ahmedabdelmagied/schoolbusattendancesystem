import 'package:flutter/material.dart';
import '../../../helpers/Constants.dart';

class DashboardCard extends StatefulWidget {
  int cardNumber;
  String cardTitle;
  Color cardColor;

  DashboardCard({this.cardNumber, this.cardTitle, this.cardColor});

  @override
  _DashboardCardState createState() => _DashboardCardState();
}

class _DashboardCardState extends State<DashboardCard> {
  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 10.0,
      color: widget.cardColor,
      child: Container(

        child: Column(
          children: <Widget>[
            new Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(20.0),
              child: Text(
                  convertToArabicNumbers(widget.cardNumber.toString()),
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 50.0),
              ),
            ),

            new Container(
              alignment: Alignment.center,
              padding: EdgeInsets.all(15.0),
              child: Text(
                widget.cardTitle,
                style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black, fontSize: 25.0),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
