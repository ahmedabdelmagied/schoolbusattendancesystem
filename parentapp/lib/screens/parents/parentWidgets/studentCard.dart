import 'package:flutter/material.dart';

import '../../../helpers/Constants.dart';

class studentCard extends StatefulWidget {
  String state, latestStation, latestStationArrival, studentName, schoolName, studentPhoto;

  studentCard(
      {this.state,
      this.latestStation,
      this.latestStationArrival,
      this.studentName,
      this.schoolName,
      this.studentPhoto});

  @override
  _studentCardState createState() => _studentCardState();
}

class _studentCardState extends State<studentCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.only(left: 40.0, right: 40.0),
        child: Card(
            elevation: 20.0,
            child: Container(
              color: Colors.grey[200],
              alignment: Alignment.center,
              child: ListView(
                children: <Widget>[
                  Container(
                    alignment: Alignment.center,
                    width: 90.0,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(left: 1.0, top: 8),
                          alignment: Alignment.center,
                          width: 150.0,
                          height: 200.0,
                          decoration: BoxDecoration(
                            image: DecorationImage(
                              image: AssetImage("assets/images/ren.jpg"),
//                                  fit: BoxFit.cover,
//                                  image: (widget.studentPhoto == null)
//                                      ? MemoryImage(base64.decode(base64String))
//                                      : MemoryImage(base64.decode(widget.studentPhoto))),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        new Container(
                            alignment: Alignment.center,
                            child: Column(
                              children: <Widget>[
                                Text(
                                  "بسنت عمرو اشرف",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.black,
                                      fontSize: 20.0),
                                ),
                              ],
                            )),
                      ],
                    ),
                  ),
                  Container(
                    child: Column(
                      children: <Widget>[
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Container(
                              margin: (MediaQuery.of(context).orientation == Orientation.portrait)
                                  ? EdgeInsets.only(top: 10.0)
                                  : EdgeInsets.only(top: 0.0),
                              alignment: Alignment.center,
                              child: Text("مدرسه النهضه التجريبيه",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                  )),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                child: Text(widget.latestStation,
                                    style: TextStyle(
                                      color: Colors.black,
                                    ))),
                            Container(
                              child: Text(" : المحطه الحاليه",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                  )),
                            ),
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Container(
                                alignment: Alignment.center,
                                child: Text("${convertToArabicNumbers("5:30")} صباحا ",
                                    textDirection: TextDirection.rtl,
                                    style: TextStyle(color: Colors.black, fontSize: 16.0))),
                            Container(
                              child: Text(" : زمن الوصول",
                                  style: TextStyle(
                                    color: Colors.black,
                                    fontSize: 16.0,
                                  )),
                            ),
                          ],
                        ),
                      ],
                    ),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                          margin: (MediaQuery.of(context).orientation == Orientation.landscape)
                              ? EdgeInsets.only(top: 0.0)
                              : EdgeInsets.only(top: 20.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text(
                                "متوقع وصول الطالب الساعه 10 صباحا",
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 17.0, color: Colors.red),
                              ),
//                    Text(
//                      " متوقع وصول الطالب الساعه  ",
//                      style:
//                      TextStyle(fontWeight: FontWeight.bold, fontSize: 15.0, color: Colors.deepOrange),
//                    ),
                            ],
                          ))
                    ],
                  ),
                ],
              ),
            )));
  }
}
