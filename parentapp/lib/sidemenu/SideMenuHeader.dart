import 'dart:convert';
import '../helpers/Constants.dart';
import 'package:flutter/material.dart';

class MenuHeader extends StatefulWidget {
  @override
  _MenuHeaderState createState() => _MenuHeaderState();
}

class _MenuHeaderState extends State<MenuHeader> {
  @override
  Widget build(BuildContext context) {
    return Container(

      decoration: new BoxDecoration(
        color: Colors.grey[300],
          border: new Border(
              bottom: new BorderSide(
                  width: 1.0, color: Colors.grey[300]))),
      padding: EdgeInsets.all(25.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Text("محمد عبد الرحمن", style: TextStyle(color: Colors.black,fontSize: 20.0, fontWeight: FontWeight.bold),),
          SizedBox(width: 25.0,),
          Expanded(
          child:CircleAvatar(
            radius: 40.0,
            backgroundImage: MemoryImage(base64.decode(base64String)),
          ),),

        ],
      ),
    );
  }
}
