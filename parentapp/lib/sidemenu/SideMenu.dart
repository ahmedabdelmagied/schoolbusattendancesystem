import 'package:flutter/material.dart';
import 'dart:async';
import '../service/connectionStatusSingleton.dart';
import '../helpers/Constants.dart';
import '../model/menuItem.dart';
import './sidemenuitem.dart';
import './SideMenuHeader.dart';
import '../screens/login_page.dart';

class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  StreamSubscription _connectionChangeStream;

  bool isOffline = false;

  @override
  initState() {
    super.initState();

    ConnectionStatusSingleton connectionStatus = ConnectionStatusSingleton.getInstance();
    _connectionChangeStream = connectionStatus.connectionChange.listen(connectionChanged);
    connectionChanged(connectionStatus.hasConnection);
  }

  void connectionChanged(dynamic hasConnection) {
    setState(() {
      isOffline = !hasConnection;
    });
  }

  int coloredIndex = 0;

  @override
  Widget build(BuildContext context) {
    return Drawer(
      semanticLabel: "label",
      child: Container(
          color: Colors.orange[400],
          child: ListView(
              //sideItems
              children: <Widget>[
                MenuHeader(
                    // TODO menu header must be set here...
                    ),

                SizedBox(height: 30),
                Container(
                  padding: EdgeInsets.all(10.0),
                  color: (coloredIndex == 0) ? Colors.white24 : Colors.orange[400],
                  child: SideItem(
                      item: MenuItem(
                          title: 'الصفحة الرئيسية',
                          onTap: () {
                            setState(() {
                              coloredIndex = 0;
                            });
                          })),
                ),

//                SizedBox(height: 10.0,),
                Container(
                  color: (coloredIndex == 1) ? Colors.white24 : Colors.orange[400],
                  padding: EdgeInsets.all(10.0),
                  child: SideItem(
                      item: MenuItem(
                          title: 'التحدث مع الاداره',
                          onTap: () {
                            setState(() {
                              coloredIndex = 1;
                            });
                          })),
                ),

//                SizedBox(height: 10.0,),
                Container(
                  padding: EdgeInsets.all(10.0),
                  color: (coloredIndex == 2) ? Colors.white24 : Colors.orange[400],
                  child: SideItem(
                      item: MenuItem(
                          title: 'تغيير كلمه السر',
                          onTap: () {
                            setState(() {
                              coloredIndex = 2;
                            });
                          })),
                ),


                Container(
                  padding: EdgeInsets.all(10.0),
                  color: (coloredIndex == 3) ? Colors.white24:Colors.orange[400],
                  child: SideItem(
                      item: MenuItem(
                          title: 'تسجيل الخروج',
                          onTap: () {
                            coloredIndex = 3;
                            Navigator.pop(context);
                            Navigator.of(context).pushAndRemoveUntil(
                                MaterialPageRoute(builder: (BuildContext context) => LoginPage()),
                                    (Route<dynamic> route) => false);

                           })),
                )
              ])),
    );
  }
}
