class Parent {
  int parentId;
  String parentNID;
  String parentName;
  String parentJob;
  String parentEmail;
  String parentAddress;

  Parent(
      {this.parentId,
      this.parentNID,
      this.parentName,
      this.parentJob,
      this.parentEmail,
      this.parentAddress});
}
