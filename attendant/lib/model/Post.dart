import 'dart:async';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Post {
  final String userId;
  final String groupId;

  Post({
    this.userId,
    this.groupId,
  });

  factory Post.fromJson(Map<String, dynamic> json) {
    return Post(
      userId: json['USER_ID'],
      groupId: json['GROUP_ID'],
    );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["userId"] = userId;
    map["groupId"] = groupId;

    return map;
  }
}

//_makeGetRequest() async {
//  // make GET request
//  String url = 'http://172.16.1.19/sbaswebapi/api/values/get';
//  Response response = await get(url);
//  // sample info available in response
//  int statusCode = response.statusCode;
//  Map<String, String> headers = response.headers;
//  String contentType = headers['content-type'];
//  String json = response.body;
//  print(json);
//}


//_makePostRequest() async {
//  // set up POST request arguments
//  String url = 'http://172.16.1.19/sbaswebapi/api/values/login';
//  Map<String, String> headers = {"Content-type": "application/json"};
//  String json = '{"userName": "عمرو", "password": "123"}';
//  // make POST request
//  Response response = await post(url, headers: headers, body: json);
//  // check the status code for the result
//  int statusCode = response.statusCode;
//  // this API passes back the id of the new item added to the body
//  String body = response.body;
//  print(response.statusCode);
//  // {
//  //   "title": "Hello",
//  //   "body": "body text",
//  //   "userId": 1,
//  //   "id": 101
//  // }
//}

Future<Post> login(String url, {Map body}) async {
  print(body);
  return http.post(url, body: body).then((http.Response response) {
    final int statusCode = response.statusCode;
    print(statusCode);
    return Post.fromJson(json.decode(response.body));
  });
}



Future<http.Response> postRequest () async {
  var url ='http://172.16.1.19/sbaswebapi/api/values/login';

  Map data = {
    'USER_NAME': 'عمرو',
    'USER_PASSWORD': '123'
  };
  //encode Map to JSON
  var body = json.encode(data);

  var response = await http.post(url,
      headers: {"Content-Type": "application/json"},
      body: body
  );
  print("${response.statusCode}");
  print("${response.body}");
  return response;
}

class MyAppLogin extends StatelessWidget {
  final Future<Post> post;

  static final LOGIN_POST_URL =
      'http://172.16.1.19/sbaswebapi/api/values/login';
  MyAppLogin({Key key, this.post}) : super(key: key);
  TextEditingController titleControler = new TextEditingController();
  TextEditingController bodyControler = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return MaterialApp(
      title: "WEB SERVICE",
      theme: ThemeData(
        primaryColor: Colors.deepOrange,
      ),
      home: Scaffold(
          appBar: AppBar(
            title: Text('Create Post'),
          ),
          body: new Container(
            margin: const EdgeInsets.only(left: 8.0, right: 8.0),
            child: new Column(
              children: <Widget>[
                new TextField(
                  controller: titleControler,
                  decoration:
                      InputDecoration(hintText: "user....", labelText: 'user'),
                ),
                new TextField(
                  controller: bodyControler,
                  decoration: InputDecoration(
                      hintText: "password....", labelText: 'password'),
                ),
                new RaisedButton(
                  onPressed: () async {
                    postRequest();
//                    Post newPost = new Post(
//                        userId: "123", id: 0, title: titleControler.text, body: bodyControler.text);
                    Map logIn = new Map();
                    logIn["userName"] = "عمرو";
                    logIn["password"] ="123";
//                    Post p = await login(LOGIN_POST_URL,
//                        body: logIn);
//                    print(p.userId +"  "+ p.groupId);
//                    Post p = await login(LOGIN_POST_URL,body:logIn );
//                    print(p.userId+"  "+p.groupId);
                  },
                  child: const Text("login"),
                )
              ],
            ),
          )),
    );
  }
}

//void main() => runApp(MyAppLogin());
