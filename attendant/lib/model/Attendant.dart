class Attendant {
  final int attendantId;
  final String attendantName;
  final String attendantPhone;
  final String attendantNID;
  final String attendantPhotoURL;
  final int busId;
   bool hasRoute = false;

  Attendant(
      {this.attendantId,
      this.attendantName,
      this.attendantPhone,
      this.attendantNID,
      this.attendantPhotoURL,
      this.hasRoute,
      this.busId});

  factory Attendant.fromJson(Map<String, dynamic> json) {
   if(json.containsKey("BUS_ROUTE_SERIAL")){
         return Attendant(
         attendantId: json["attendantObj"]["ATTENDANT_ID"],
         attendantName: json["attendantObj"]["ATTENDANT_NAME"],
         attendantPhone: '${json["attendantObj"]["ATTENDANT_PHONE"]}',
         attendantNID: '${json["attendantObj"]["ATTENDANT_NID"]}',
         attendantPhotoURL: json["attendantObj"]["ATTENDANT_PHOTO"],
           hasRoute: json.containsKey("BUS_ROUTE_SERIAL"),
          busId: json["BUS_ID"]
         );
   }
   return Attendant(
       attendantId: json["ATTENDANT_ID"],
       attendantName: json["ATTENDANT_NAME"],
       attendantPhone: '${json["ATTENDANT_PHONE"]}',
       attendantNID: '${json["ATTENDANT_NID"]}',
       attendantPhotoURL: json["ATTENDANT_PHOTO"],

   );
  }

  Map toMap() {
    var map = new Map<String, dynamic>();
    map["ATTENDANT_ID"] = attendantId;
    map["ATTENDANT_NAME"] = attendantName;
    map["ATTENDANT_PHONE"] = attendantPhone;
    map["ATTENDANT_NID"] = attendantNID;
    map["ATTENDANT_PHOTO"] = attendantPhotoURL;

    return map;
  }
}
