import 'package:flutter/cupertino.dart';

class MenuItem {
  String title;
   Function onTap;
  Icon icon;

  MenuItem({this.title, this.onTap});
}
