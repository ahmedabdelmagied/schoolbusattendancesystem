class School {
  int schoolId;
  String schoolName;
  String schoolLongitude;
  String schoolLatitude;
  int eduLevel;

  School(
      {this.schoolId,
      this.schoolName,
      this.schoolLongitude,
      this.schoolLatitude,
      this.eduLevel});
}
