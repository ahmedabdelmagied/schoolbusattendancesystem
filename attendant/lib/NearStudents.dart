import 'dart:convert';

import 'package:attendant/widget/Dialog.dart' as prefix0;
import 'package:attendant/widget/StudentDialog.dart';
import 'package:attendant/widget/sidemenu/SideMenu.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:attendant/arriveBus.dart';
import 'package:attendant/LoginPage.dart';
import 'package:attendant/sevice/GlobalState.dart';

import 'helpers/Constants.dart';
import 'model/Student.dart';
import 'widget/Dialog.dart';

class NearStudent extends StatefulWidget {
  @override
  NearStudentState createState() => NearStudentState();
}

class NearStudentState extends State<NearStudent> {
  List<Student> students = [];

  int availabileCapacity = 0;
  int pickedUpNumber = 0;
  String barcode;
  GlobalState _store = GlobalState.instance;

  final SlidableController slidableController = SlidableController();

  NearStudentState() {
    loadStudentsInBus();
  }

  @override
  Widget build(BuildContext context) {
//    students.sort((a, b) => a.remainningStations.compareTo(b.remainningStations));

    return Scaffold(
      backgroundColor: appDarkGreyColor,
      appBar: AppBar(
        leading: new Container(),
        backgroundColor: appGreyColor,
        title: Text("SBUS"),
      ),
      resizeToAvoidBottomPadding: false,
      endDrawer:
      Drawer(
        // Add a ListView to the drawer. This ensures the user can scroll
        // through the options in the drawer if there isn't enough vertical
        // space to fit everything.
        child: ListView(
          // Important: Remove any padding from the ListView.
          padding: EdgeInsets.zero,
          children: <Widget>[
//            DrawerHeader(
//              child: Text('Drawer Header'),
//              decoration: BoxDecoration(
//                color: Colors.blue,
//              ),
//            ),
            ListTile(
              title: Text('التحدث مع الاداره', style:TextStyle(fontSize: 20.0 ,),textDirection: TextDirection.rtl,),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('التحدث مع ولى الامر', style:TextStyle(fontSize: 20.0),textDirection: TextDirection.rtl),
              onTap: () {
                // Update the state of the app
                // ...
                // Then close the drawer
                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('تغير الرقم السرى',  style:TextStyle(fontSize: 20.0 ,),textDirection: TextDirection.rtl),
              onTap: () {

                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('وصول الخط', style:TextStyle(fontSize: 20.0 ,), textDirection: TextDirection.rtl),
              onTap: () {
                Navigator.push(context, new MaterialPageRoute(builder: (BuildContext) => new ArriveBus()));
//                Navigator.pop(context);
              },
            ),
            ListTile(
              title: Text('تسجيل الخروج', style:TextStyle(fontSize: 20.0 ,) ,textDirection: TextDirection.rtl),
              onTap: () {
                Navigator.push(context , new MaterialPageRoute(builder: (BuildContext) => new LoginPage()));
//                Navigator.pop(context);
              },
            ),
          ],
        ),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SizedBox(
            height: 15,
          ),
          Container(
              child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              RaisedButton(
                padding: EdgeInsets.all(15),
                onPressed: () async {
                  /////////////////////////////////
                  int busCapacity = _store.get('bus-capacity');
                  bool rideFlag = busCapacity > 0;
                  String url = '${baseURL}/scanstudent';

                  int studentId = 1;
                  var body = json.encode({
                    'STUDENT_ID': studentId,
                    'BUS_ROUTE_ID': _store.get('bus').routeSerial,
                    'ROUTE_ID': _store.get('bus').routeId,
                    'RIDEFLAG': rideFlag
                  });

                  var response = await http.post(url,
                      headers: {"Content-Type": "application/json"},
                      body: body);
                  var responseData = json.decode(response.body);

                  print(responseData);
                  if (responseData['message'] == "The Student Pickedup") {
                    responseData['STUDENT']['STOP_ID'] =
                        responseData['STOP_ID'];
                    responseData['STUDENT']['STOP_NAME'] =
                        responseData['STOP_NAME'];
                    responseData['STUDENT']['LONGITUDE'] =
                        responseData['LONGITUDE'];
                    responseData['STUDENT']['LATITUDE'] =
                        responseData['LATITUDE'];
                    Student tempStudent =
                        Student.fromJson(responseData['STUDENT']);
                    tempStudent.pickedUpSerial =
                        responseData['PICKED_UP_STUDENT_SERIAL'];
                    setState(() {
                      Student.studentsList.add(tempStudent);
                      availabileCapacity--;
                      pickedUpNumber++;
                    });
                    busCapacity--;
                    _store.set('bus-capacity', busCapacity);
                  }
                  if (responseData['message'] ==
                      "The Student Already Pickedup") {
                    int pickedUpStudentSerial =
                        responseData['PICKED_UP_STUDENT_SERIAL'];

                    ConfirmAction ans = await Dialogs.ConfirmDialog(
                        context: context,
                        msg: "الطالب بالفعل راكب هل تريد انزاله؟");
                    if (ans == ConfirmAction.ACCEPT) {
                      String dropURL = '${baseURL}/dropstudent';
                      var dropBody = json.encode({
                        'PICKED_UP_STUDENT_SERIAL': pickedUpStudentSerial,
                      });

                      var responseDrop = await http.post(dropURL,
                          headers: {"Content-Type": "application/json"},
                          body: dropBody);
                      var responseDropData = json.decode(responseDrop.body);
                      if (responseDropData['message'] ==
                          'The student has been dropped') ;
                      busCapacity++;
                      removeStudentFromList(
                          studentId, Student.studentsList); //barcode
                      setState(() {
                        availabileCapacity++;
                        pickedUpNumber--;
                      });
                      _store.set('bus-capacity', busCapacity);
                    }
                  }
                },
                child: Image(
                  image: AssetImage('assets/images/qr-code.png'),
                  width: 75,
                  height: 75,
                ),
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          _store.get('bus').capacity.toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "  : عدد المقاعد",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          pickedUpNumber.toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "  : عدد الركاب",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Text(
                          availabileCapacity.toString(),
                          style: TextStyle(color: Colors.white),
                        ),
                        Text(
                          "  : عدد المقاعد المتاحة",
                          style: TextStyle(color: Colors.white),
                        )
                      ],
                    ),
                  ],
                ),
              )
            ],
          )),
          SizedBox(
            height: 25,
          ),
          Flexible(
            child: Container(
              child: ListView(
                  children: Student.studentsList
                      .map((data) => _buildListItem(context, data))
                      .toList()),
            ),
          )
        ],
      ),
    );
  }

  Widget _buildListItem(BuildContext context, Student student) {
    return Slidable(
      key: Key(student.studentName),
      controller: slidableController,
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.25,
      child: Directionality(
        textDirection: TextDirection.rtl,
        child: Card(
          key: ValueKey(student.studentName),
          elevation: 8.0,
          margin: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 6.0),
          child: Container(
            decoration: BoxDecoration(color: Color.fromRGBO(64, 75, 96, .9)),
            child: ListTile(
              contentPadding:
                  EdgeInsets.symmetric(horizontal: 20.0, vertical: 10.0),
              title: Text(
                student.studentName,
                style:
                    TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
              ),
              subtitle: Row(
                children: <Widget>[
                  new Flexible(
                      child: new Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                        RichText(
                          text: TextSpan(
                            text: student.stopStation.stopName +
                                "     " +
                                student.remainningStations.toString(),
                            style: TextStyle(color: Colors.white),
                          ),
                          maxLines: 3,
                          softWrap: true,
                        )
                      ]))
                ],
              ),
              trailing: Container(
                  padding: EdgeInsets.only(right: 12.0),
                  decoration: new BoxDecoration(
                      border: new Border(
                          right: new BorderSide(
                              width: 1.0, color: Colors.white24))),
                  child: Hero(
                      tag: "avatar_" + student.studentName,
                      child: CircleAvatar(
                        radius: 32,
                        backgroundImage: MemoryImage(student.photo),
                      ))),
              onTap: () {
                showDialog(
                  context: context,
                  builder: (BuildContext context) => StudentDialog(
                    student: student,
                    studentsList: Student.studentsList,
                    parent: this,
                  ),
                );
                setState(() {
                  Student.studentsList.length;
                });
              },
            ),
          ),
        ),
      ),
      secondaryActions: <Widget>[
        IconSlideAction(
            caption: 'نزول',
            color: Colors.red,
            icon: Icons.exit_to_app,
            onTap: () {
              showDialog(
                context: context,
                builder: (BuildContext context) => StudentDialog(
                  student: student,
                  studentsList: Student.studentsList,
                  parent: this,
                ),
              );
              setState(() {
                Student.studentsList.length;
              });
            }),
      ],
    );
  }

  Future scan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        this.barcode = 'The user did not grant the camera permission!';
      } else {
        this.barcode = 'Unknown error: $e';
      }
    } on FormatException {
      this.barcode =
          'null (User returned using the "back"-button before scanning anything. Result)';
    } catch (e) {
      this.barcode = 'Unknown error: $e';
    }
  }

  loadStudentsInBus() async {
    final response = await http.get('${baseURL}/getpickedupstudents/1',
        headers: {"Content-type": "application/json"});
    var responseData = json.decode(response.body);
    for (var studentJson in responseData) {
      Student student = Student.fromJson(studentJson);
      setState(() {
        Student.studentsList.add(student);
      });
    }
    int tempCapacity = _store.get('bus-capacity');
    tempCapacity -= Student.studentsList.length;
    setState(() {
      availabileCapacity = tempCapacity;
      pickedUpNumber = _store.get('bus').capacity - availabileCapacity;
    });
    _store.set('bus-capacity', tempCapacity);
  }

  removeStudentFromList(int id, List<Student> lst) {
    for (int i = 0; i < lst.length; i++) {
      if (lst[i].studentId == id) {
        setState(() {
          lst.removeAt(i);
        });
        break;
      }
    }
  }
}
