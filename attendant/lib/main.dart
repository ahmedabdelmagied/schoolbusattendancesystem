import 'package:attendant/NearStudents.dart';
import 'package:flutter/material.dart';
import 'ConfiemBus.dart';
import 'LoginPage.dart';
import 'helpers/Constants.dart';
import 'model/Post.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  final routes = <String, WidgetBuilder>{
    loginPageTag: (context) => LoginPage(),
    confirmBusTag: (context) => ConfirmBus(),

  };

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'QR Generator-Scanner',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(),

      routes: routes,
    );
  }
}
