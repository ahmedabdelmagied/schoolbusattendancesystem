import 'dart:convert';
import 'package:barcode_scan/barcode_scan.dart';
import 'package:flutter/material.dart';
import 'NearStudents.dart';
import 'helpers/Constants.dart';
import 'model/Bus.dart';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as http;
import 'sevice/GlobalState.dart';

class ArriveBus extends StatefulWidget {
  @override
  _ArriveBusState createState() => _ArriveBusState();
}

class _ArriveBusState extends State<ArriveBus> {
  GlobalState _store = GlobalState.instance;
  String barcode = "";
  Bus bus = Bus(
      routeId: '',
      routeSerial: '',
      busRouteStartTime: '',
      busDriverEndDate: '',
      busDriverDriveDate: '',
      busDriverId: '',
      busDriverSerial: '',
      capacity: 0,
      modelName: '',
      colorName: '',
      busNumber: '',
      busId: '',
      busRouteArrivalTime: '',
      busDriverName: '');

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: appDarkGreyColor,
        appBar: AppBar(
          leading: new Container(),
          backgroundColor: appGreyColor,
          title: Text("SBAS"),
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              new Container(
                height: 75,
                width: 175,
                child: RaisedButton(
//                    color: appGreyColorLight,
                    padding: EdgeInsets.all(15),
                    onPressed: () async {
                      final response = await http.get(
                          '${baseURL}/busdetails?id=1',
                          headers: {"Content-type": "application/json"});
                      print(response.statusCode);
                      print(response.body);
                      if (response.statusCode == 200) {
                        // If the call to the server was successful, parse the JSON.
                        print(response.body);
                        setState(() {
                          bus = Bus.fromJson(json.decode(response.body));
                          _store.set('bus', bus);
                          _store.set('bus-capacity', bus.capacity);
                        });
                      } else {
                        // If that call was not successful, throw an error.
                        throw Exception('Failed to load post');
                      }
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: <Widget>[
                        Image(
                          image: AssetImage('assets/images/qr-code.png'),
                          width: 75,
                          height: 75,
                        ),
                        Text("Scan")
                      ],
                    )),
              ),
              SizedBox(
                height: 25,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    bus.busNumber,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "رقم الاتوبيس : ",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    bus.colorName,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "لون الاتوبيس : ",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    bus.busDriverName,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "سائق الاتوبيس : ",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "${_store.get('attendant').attendantName}",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "مرافق الاتوبيس : ",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    bus.routeSerial,
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                  Text(
                    "رقم الخط : ",
                    textDirection: TextDirection.rtl,
                    style: TextStyle(color: Colors.white),
                  ),
                ],
              ),
              SizedBox(
                height: 25,
              ),
              RaisedButton(
                onPressed: () async {
                  String url = '${baseURL}/stopendroute';
                  var body = json.encode({
                    'ATTENDANT_ID': _store.get('attendant').attendantId,
                    'BUS_ROUTE_ID': bus.routeSerial,
                    'START_FLAG': true
                  });

                  var response = await http.post(url,
                      headers: {"Content-Type": "application/json"},
                      body: body);
                  var responseDate = json.decode(response.body);
                  print(response.body);
                  print(responseDate );
                  if (responseDate['success'] == 'true') {
                    print("here");
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => NearStudent()),
                    );
                  }
                },
                child: Text("حفظ"),
              )
            ]));
  }

  Future scan() async {
    try {
      var barcode = await BarcodeScanner.scan();
      setState(() => this.barcode = barcode);
    } on PlatformException catch (e) {
      if (e.code == BarcodeScanner.CameraAccessDenied) {
        this.barcode = 'The user did not grant the camera permission!';
      } else {
        this.barcode = 'Unknown error: $e';
      }
    } on FormatException {
      this.barcode =
      'null (User returned using the "back"-button before scanning anything. Result)';
    } catch (e) {
      this.barcode = 'Unknown error: $e';
    }
  }
}
