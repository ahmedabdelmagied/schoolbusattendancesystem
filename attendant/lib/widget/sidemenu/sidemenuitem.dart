import '../../model/menuItem.dart';
import 'package:flutter/material.dart';

class SideItem extends StatefulWidget {
  MenuItem item;

  @override
  _SideItemState createState() => _SideItemState();

  SideItem({this.item});
}

class _SideItemState extends State<SideItem> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
        title: Text(
          widget.item.title,
          style: TextStyle(
            fontSize: 20.0,
          ),
          textDirection: TextDirection.rtl,
        ),
        onTap: widget.item.onTap);
  }
}
