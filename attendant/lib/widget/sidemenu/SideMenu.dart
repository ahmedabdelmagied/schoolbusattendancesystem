import 'package:attendant/model/menuItem.dart';
import 'package:attendant/widget/sidemenu/sidemenuitem.dart';
import 'package:flutter/material.dart';

import '../../arriveBus.dart';

class SideMenu extends StatefulWidget {
  @override
  _SideMenuState createState() => _SideMenuState();
}

class _SideMenuState extends State<SideMenu> {
  @override
  Widget build(BuildContext context) {
    print('in Drawer');
    return Drawer(
        child: ListView(
            // Important: Remove any padding from the ListView.
            padding: EdgeInsets.zero,
            children: <Widget>[
          SideItem(
              item: MenuItem(
                  title: 'التحدث مع الاداره', onTap: () => print('testttt'))),
          SideItem(
              item: MenuItem(
                  title: 'التحدث مع ولى الامر',
                  onTap: () => Navigator.pop(context))),
          SideItem(
              item: MenuItem(
                  title: 'تغيير الرقم السرى',
                  onTap: () => Navigator.pop(context))),
          SideItem(
              item: MenuItem(
                  title: 'وصول الخط',
                  onTap: () {
                    print('test');
                    Navigator.push(context, new MaterialPageRoute(builder: (BuildContext) => new ArriveBus()));
                    print('here');
                Navigator.pop(context);
                  })),
          SideItem(
              item: MenuItem(
                  title: 'تسجيل الخروج', onTap: () => Navigator.pop(context))),
        ]));
  }
}
