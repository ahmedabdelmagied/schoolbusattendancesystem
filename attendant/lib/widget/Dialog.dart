import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class Dialogs {
  static Future<ConfirmAction> ConfirmDialog(
      {BuildContext context, String msg}) async {
    return showDialog<ConfirmAction>(
      context: context,
      barrierDismissible: false, // user must tap button for close dialog!
      builder: (BuildContext context) {
        return AlertDialog(
          title: Directionality(
              textDirection: TextDirection.rtl, child: Text('تأكيد')),
          content: Directionality(
              textDirection: TextDirection.rtl, child: Text(msg)),
          actions: <Widget>[
            FlatButton(
              child: const Text('لا'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.CANCEL);
              },
            ),
            FlatButton(
              child: const Text('نعم'),
              onPressed: () {
                Navigator.of(context).pop(ConfirmAction.ACCEPT);
              },
            )
          ],
        );
      },
    );
  }

  static void showDialogBox({BuildContext context, String msg}) {
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Directionality(
              textDirection: TextDirection.rtl, child: Text("تحذير")),
          content: Directionality(
              textDirection: TextDirection.rtl, child: Text(msg)),
          actions: <Widget>[
            // usually buttons at the bottom of the dialog
            FlatButton(
              child: Directionality(
                  textDirection: TextDirection.rtl, child: Text("اغلاق")),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }
}

enum ConfirmAction { CANCEL, ACCEPT }
