import 'dart:convert';

import 'package:attendant/NearStudents.dart';
import 'package:attendant/sevice/GlobalState.dart';

import '../helpers/Constants.dart';
import '../model/Student.dart';
import 'Dialog.dart';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;

class Consts {
  Consts._();

  static const double padding = 16.0;
  static const double avatarRadius = 55.0;
}

class StudentDialog extends StatefulWidget {
  final Student student;
  List<Student> studentsList;
  NearStudentState parent;

  StudentDialog({this.student, this.studentsList, this.parent});

  @override
  _StudentDialogState createState() => _StudentDialogState();
}

class _StudentDialogState extends State<StudentDialog> {
  GlobalState _store = GlobalState.instance;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(Consts.padding),
      ),
      elevation: 0.0,
      backgroundColor: Colors.white,
      child: dialogContent(context, widget.student, widget.studentsList),
    );
  }

  dialogContent(BuildContext context, Student student, List<Student> lst) {
    return Stack(
      children: <Widget>[
        // Image of the user
        Positioned(
          left: Consts.padding,
          right: Consts.padding,
          top: 16.0,
          child: new Container(
            width: 100.0,
            height: 160.0,
            decoration: new BoxDecoration(
              shape: BoxShape.circle,
              image: new DecorationImage(
                fit: BoxFit.contain,
                  image: MemoryImage(student.photo),
              ),
            ),
          ),
        ),
        new Container(
          padding: EdgeInsets.only(
            top: 140.0,
            bottom: Consts.padding,
            left: Consts.padding,
            right: Consts.padding,
          ),
          margin: EdgeInsets.only(top: Consts.avatarRadius),
          child: new Column(
            children: <Widget>[
              Padding(
                child: Text(
                  student.studentName,
                  style: TextStyle(
                      fontSize: 20.0,
                      fontWeight: FontWeight.bold,
                      color: Color.fromARGB(255, 0, 147, 112)),
                ),
                padding: EdgeInsets.only(bottom: 10.0),
              ),
              Divider(
                height: 25.0,
                color: Color.fromARGB(255, 0, 147, 132),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Container(
                          margin: EdgeInsets.only(bottom: 5.0),
                          child: new Text(student.studentNID.toString(),
                              style: TextStyle(
                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black54)),
                        ),
                        new Text("  : الرقم القومى",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 0, 147, 132))),
                      ]),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text(student.studentSchool.schoolName,
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                        new Container(
                          child: Row(
                            children: <Widget>[
                              new Text("  :  مدرسة ",
                                  style: TextStyle(
                                      fontSize: 15.0,
                                      fontWeight: FontWeight.bold,
                                      color: Color.fromARGB(255, 0, 147, 132))),
                            ],
                          ),
                        ),
                      ]),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Text(student.stopStation.stopName,
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                        new Text("  : المحطة",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 0, 147, 132))),
                      ]),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          child: new Text(student.studentMobile,
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black54)),
                        ),
                        new Text("  :  رقم الموبايل",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 0, 147, 132))),
                      ]),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(bottom: 5.0),
                          child: new Text(student.studentClass,
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black54)),
                        ),
                        new Text("  :  السنة الدراسية ",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 0, 147, 132))),
                      ]),
                ],
              ),
              SizedBox(
                height: 15.0,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        new Container(
                          margin: EdgeInsets.only(bottom: 5.0),
                          child: new Text("5",
                              style: TextStyle(
                                  fontSize: 15.0,
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black54)),
                        ),
                        new Text("  : عدد المحطات ",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Color.fromARGB(255, 0, 147, 132))),
                      ]),
                ],
              ),
              SizedBox(
                height: 20.0,
              ),
              Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    new RaisedButton(
                        color: Color.fromARGB(255, 0, 147, 132),
                        child: new Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Icon(
                              Icons.exit_to_app,
                              color: Colors.white,
                            ),
                            SizedBox(
                              width: 15.0,
                            ),
                            new Container(
                              alignment: Alignment.center,
                              child: Text(
                                "تأكيد",
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  fontSize: 18.0,
                                  color: Colors.white,
                                ),
                              ),
                            ),
                          ],
                        ),
                        onPressed: () async {
                          ConfirmAction ans = await Dialogs.ConfirmDialog(
                              context: context,
                              msg: "الطالب بالفعل راكب هل تريد انزاله؟");
                          if (ans == ConfirmAction.ACCEPT) {
                            String dropURL = '${baseURL}/dropstudent';
                            var dropBody = json.encode({
                              'PICKED_UP_STUDENT_SERIAL':
                                  student.pickedUpSerial,
                            });

                            var responseDrop = await http.post(dropURL,
                                headers: {"Content-Type": "application/json"},
                                body: dropBody);
                            var responseDropData =
                                json.decode(responseDrop.body);
                            if (responseDropData['message'] ==
                                'The student has been dropped') ;
                            int index = Student.getStudentIndexFromList(
                                student.studentId, lst);
                            widget.parent.setState(() {
                              Student.studentsList.removeAt(index);
                              widget.parent.pickedUpNumber--;
                              widget.parent.availabileCapacity++;
                              _store.set('bus-capacity',
                                  widget.parent.availabileCapacity);
                            });
                            setState(() {
                              Navigator.pop(context);
                            });
                          }
                        })
                  ])
            ],
          ),
        )
      ],
    );
  }
}

dialogContent(BuildContext context, Student student) {
  return Stack(
    children: <Widget>[
      // Image of the user
      Positioned(
        left: Consts.padding,
        right: Consts.padding,
        top: 16.0,
        child: new Container(
          width: 100.0,
          height: 160.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            image: new DecorationImage(
              fit: BoxFit.contain,
              image: MemoryImage(student.photo),
            ),
          ),
        ),
      ),
      new Container(
        padding: EdgeInsets.only(
          top: 140.0,
          bottom: Consts.padding,
          left: Consts.padding,
          right: Consts.padding,
        ),
        margin: EdgeInsets.only(top: Consts.avatarRadius),
        child: new Column(
          children: <Widget>[
            Padding(
              child: Text(
                student.studentName,
                style: TextStyle(
                    fontSize: 20.0,
                    fontWeight: FontWeight.bold,
                    color: Color.fromARGB(255, 0, 147, 112)),
              ),
              padding: EdgeInsets.only(bottom: 10.0),
            ),
            Divider(
              height: 25.0,
              color: Color.fromARGB(255, 0, 147, 132),
            ),
//            Padding(
//              padding: EdgeInsets.only(bottom: 20.0),
//            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        margin: EdgeInsets.only(bottom: 5.0),
                        child: new Text(student.studentNID.toString(),
                            style: TextStyle(
                                fontSize: 14.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                      ),
                      new Text("  : الرقم القومى",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 147, 132))),
                    ]),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(student.studentSchool.schoolName,
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54)),
                      new Container(
                        child: Row(
                          children: <Widget>[
                            new Text("  :  مدرسة ",
                                style: TextStyle(
                                    fontSize: 15.0,
                                    fontWeight: FontWeight.bold,
                                    color: Color.fromARGB(255, 0, 147, 132))),
                          ],
                        ),
                      ),
                    ]),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(student.stopStation.stopName,
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Colors.black54)),
                      new Text("  : المحطة",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 147, 132))),
                    ]),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        child: new Text(student.studentMobile,
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                      ),
                      new Text("  :  رقم الموبايل",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 147, 132))),
                    ]),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(bottom: 5.0),
                        child: new Text(student.studentClass,
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                      ),
                      new Text("  :  السنة الدراسية ",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 147, 132))),
                    ]),
              ],
            ),
            SizedBox(
              height: 15.0,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: <Widget>[
                Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Container(
                        margin: EdgeInsets.only(bottom: 5.0),
                        child: new Text("5",
                            style: TextStyle(
                                fontSize: 15.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.black54)),
                      ),
                      new Text("  : عدد المحطات ",
                          style: TextStyle(
                              fontSize: 15.0,
                              fontWeight: FontWeight.bold,
                              color: Color.fromARGB(255, 0, 147, 132))),
                    ]),
              ],
            ),
            SizedBox(
              height: 20.0,
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: <Widget>[
              new RaisedButton(
                  color: Color.fromARGB(255, 0, 147, 132),
                  child: new Row(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Icon(
                        Icons.exit_to_app,
                        color: Colors.white,
                      ),
                      SizedBox(
                        width: 15.0,
                      ),
                      new Container(
                        alignment: Alignment.center,
                        child: Text(
                          "تأكيد",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18.0,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ],
                  ),
                  onPressed: () {
//                    disp();

//                    prefix0.ConfirmAction ans =
//                        await prefix0.Dialog.ConfirmDialog(
//                        context: context,
//                        msg: "الطالب بالفعل راكب هل تريد انزاله؟");
//                    if (ans == prefix0.ConfirmAction.ACCEPT) {
//                      String dropURL = '${baseURL}/dropstudent';
//                      var dropBody = json.encode({
//                        'PICKED_UP_STUDENT_SERIAL': pickedUpStudentSerial,
//                      });
//
//                      var responseDrop = await http.post(dropURL,
//                          headers: {"Content-Type": "application/json"},
//                          body: dropBody);
//                      var responseDropData = json.decode(responseDrop.body);
//                      if (responseDropData['message'] ==
//                          'The student has been dropped') ;
//                      busCapacity++;
//                      removeStudentFromList(studentId); //barcode
//                      setState(() {
//                        availabileCapacity++;
//                        pickedUpNumber--;
//                      });
//                      _store.set('bus-capacity', busCapacity);
//                    }
                  })
            ])
          ],
        ),
      )
    ],
  );
}
