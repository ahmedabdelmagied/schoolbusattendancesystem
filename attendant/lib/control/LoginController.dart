import 'package:attendant/helpers/Constants.dart';
import 'package:attendant/model/Attendant.dart';
import 'package:http/http.dart' as http;
import 'dart:async';
import 'dart:convert';

class Login {
  static Future<Attendant> login({String url, Map requestBody}) async {
    var absoluteURL = '${baseURL}${url}';

    //encode Map to JSON {request body}
    var body = json.encode(requestBody);

    var response = await http.post(absoluteURL,
        headers: {"Content-Type": "application/json"}, body: body);
    print("${response.statusCode}");
    print("${response.body}");
    if (response.statusCode==200)
    return Attendant.fromJson(json.decode(response.body));
    else{
      return null;
    }
  }
}
