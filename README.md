Smart Bus Attendance System 
---
  + system that gives the comfort and controll of a well designed system for transporting children from home to school and back
    the system consists of 2 mobile application and web application 
       + 2 Application one for `Bus supervisor` other for `Bus Driver` 
       + web application is for `Administration` to follow the process of moving and riding little children.
---
  + mobile application is developed using `flutter` 
  + web application is developed using `asp.net mvc`
  + database is `SQL database`
---

Bus Supervisor app
---

Supervisor should enter the username and password to enter the app
<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69815912-0451ef80-1200-11ea-8d5c-e056a8c1ced6.png" width="200" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69815915-05831c80-1200-11ea-8044-fe36576df7bf.png" width="200" height="420">
<p>

---
for the first time Bus Supervisor should Scan the Bus by this way adminstor will know the time supervisor arrived to bus

<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69816314-ca351d80-1200-11ea-86b5-8d1647e60c6b.png" width="200" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69816325-cbfee100-1200-11ea-9a79-df08dc6a65b4.png" width="200" height="420">
<p>

---
For every incomming student, Bus Supervisor should scan bar-code student hold and confirm the enterence of the students
<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69816468-21d38900-1201-11ea-85ba-a641f79b5e43.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69816470-2435e300-1201-11ea-88a2-d9e34f6501aa.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69816472-25671000-1201-11ea-9c2f-549f993fc78f.png" width="230" height="420">
<p>

---
Bus Supervisor after student reached his station can scan his bar-code in order to get off the bus
<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69817294-f2258080-1202-11ea-8e24-91429bbc191a.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69817298-f356ad80-1202-11ea-84f4-b62e54e49cfc.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69817300-f5207100-1202-11ea-820f-4f8774797b10.png" width="230" height="420">
<p>

---

App has other options most not implemented yet. 
<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69817559-8a236a00-1203-11ea-8f36-3a3fa6e7bcdc.png" width="230" height="420">
<p>

Driver app
---
At first driver enter the username and password and after that scan the bus by this way the adminstor know the time supervisor arrived to bus
<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69817934-5e54b400-1204-11ea-803e-de434edf9da8.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69817935-5f85e100-1204-11ea-98cd-a4d0f79c3315.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69817936-60b70e00-1204-11ea-8fc9-65a39c32a26b.png" width="230" height="420">
<p>

---
After scanning the bus google maps open with a route and stations driver should stops at

<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69818219-f357ad00-1204-11ea-859c-53b6f10d1d2a.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69818222-f5217080-1204-11ea-8038-0c932eaf63bf.png" width="230" height="420">
<p>
---
for every station the driver arrived he should confirm the arrival
<p align="center">
<img src="https://user-images.githubusercontent.com/25884693/69818346-3c0f6600-1205-11ea-8cec-62f381936527.png" width="230" height="420">
<img src="https://user-images.githubusercontent.com/25884693/69818348-3ca7fc80-1205-11ea-9bbf-a3df510954d7.png" width="230" height="420">
<p>




